1on1
====

# Usage

## 1. Register Device
If you don't have a client_id & client_secret saved on local device:
    - Do a POST on ```/api/v1/oauth/v2/register```
    - e.g. response: ```{"client_id":"1_1x9t8tfuxwcg04ow04cso0kskkkcckk4w8gsswgogs44wk8800","client_secret":"cvrgjuxqktcggo400cc0wkk0kg0kssscco8wwcg4g04wo4gc4"}```
    - Save the 2 values locally and use them on step 2

## 2. Get Access Token
After authorizing with facebook:
    - Do a POST on ```/api/v1/oauth/v2/token/facebook```
    - Set header ```Authorization``` with value ```Basic {AUTH}``` where Auth is base64Encode({client_id}:{client_secret})
    - Set the body:
    ```
    {
        "grant_type": "password",
        "username": "102577913587512",
        "password": "EAAaO1BS8OpcBAGS3zEuUS2kMIDkT8vnnOJeKYnkZB6ZCWZA6o3jnbLWNUaTIZAfqYKtS4iADO6cWi54ZCObZAwmE9kxHaXzM5ff4ZAwArl2ItSt1brRpEJHn9U1e1EZCOI0dK2AZBG9PgsZCcCvKc8tM9EGOJU9SIMagS08ttro5Ez3TaEThD8ZB2mDrWDdDiSLeMkN2T9ggR9ZB0aJbJqhSRWK5"
    }
    ```
        - ```grant_type``` is always ```password```
        - ```username``` is the user's facebook id
        - ```password``` is the user's facebook access token

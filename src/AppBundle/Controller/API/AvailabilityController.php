<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\Availability;
use AppBundle\Entity\WeekDay;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AvailabilityController extends BaseController
{

    const WEEK_DAYS = 'weekdays';
    const WEEK_END  = 'weekend';

    /**
     *
     * @Get("/availability")
     *
     * @return JsonResponse
     */
    public function availabilityAction(Request $request)
    {
        $user = $this->getUser();
        $availabilityIntervals = $this->getAvailabilityRepo()->findByUser($user);

        $returnIntervals = [];

        foreach ($availabilityIntervals as $interval) {
            $returnIntervals[$interval->getWeekDay()->getId()][] = [
                'start' => (int) $interval->getStart(),
                'end'   => (int) $interval->getEnd()
            ];
        }

        return new JsonResponse($returnIntervals);
    }

    /**
     *
     * @Get("/availability/settings")
     *
     * @return JsonResponse
     */
    public function availabilitySettingsAction(Request $request)
    {
        $user = $this->getUser();
        $availabilityIntervals = $this->getAvailabilityRepo()->findByUser($user);


        $returnDayIntervals = [];

        foreach ($availabilityIntervals as $interval) {
            $weekDayId = $interval->getWeekDay()->getId();

            $returnDayIntervals[$interval->getWeekDay()->getId()][] = [
                'start' => (int) $interval->getStart(),
                'end'   => (int) $interval->getEnd()
            ];
        }

        $intervals = [];

        foreach ($returnDayIntervals as $day=>$interval) {

            if ($day >= WeekDay::MONDAY && $day <= WeekDay::FRIDAY) {
                if (empty($intervals[self::WEEK_DAYS])) {
                    $intervals[self::WEEK_DAYS] = $interval;
                }
                continue;
            } else {
                if (empty($intervals[self::WEEK_END])) {
                    $intervals[self::WEEK_END] = $interval;
                }
                continue;
            }
        }

        $returnIntervals = [];
        foreach($intervals as $period=>$interval) {
            $returnIntervals[] = [
                'period' => $period,
                'intervals' => $interval
            ];
        }

        return new JsonResponse($returnIntervals);
    }

   /**
     *
     * @Get("/users/availability/{userId}", requirements={"userId" = "\d+"})
     *
     * @return JsonResponse
     */
    public function userAvailabilityAction(Request $request, $userId) {
        $user = $this->getUserRepo()->findOneById($userId);

        $availabilityIntervals = $this->getAvailabilityRepo()->findByUser($user);

        $returnIntervals = [];

        foreach ($availabilityIntervals as $interval) {
            $returnIntervals[$interval->getWeekDay()->getId()][] = [
                'start' => (int) $interval->getStart(),
                'end'   => (int) $interval->getEnd()
            ];
        }

        return new JsonResponse($returnIntervals);
    }

   /**
     *
     * @Post("/availability/onboarding")
     *
     * @return JsonResponse
     */
    public function newOnboardingAvailabilityAction(Request $request)
    {
        $period    = $request->get('period');
        $intervals = $request->get('intervals');

        $days = [];

        if ($period == self::WEEK_DAYS) {
            $days = [
                WeekDay::MONDAY,
                WeekDay::TUESDAY,
                WeekDay::WEDNESDAY,
                WeekDay::THURSDAY,
                WeekDay::FRIDAY
            ];
        } else if ($period == self::WEEK_END) {
            $days = [
                WeekDay::SATURDAY,
                WeekDay::SUNDAY
            ];
        }

        foreach ($days as $day) {
            $response = $this->forward(
                'AppBundle\Controller\Api\AvailabilityController::newDayAvailabilityAction',
                [
                    'day'       => $day,
                    'intervals' => $intervals
                ]
            );
        }

        return new JsonResponse('SAVED');
    }

   /**
     *
     * @Post("/availability")
     *
     * @return JsonResponse
     */
    public function newDayAvailabilityAction(Request $request)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        $dayId = $request->get('day');
        $day = $this->getWeekDayRepo()->findOneById($dayId);

        $intervals = $request->get('intervals');

        if (!isset($day)) {
            return new Response('Wrong day.', Response::HTTP_NOT_FOUND);
        }

        $deleteAvailabilityQuery = "   DELETE
                                         FROM availability
                                        WHERE user_id = " . $userId . " AND
                                              week_day_id = " . $dayId;

        $dbConn = $this->getEntityManager()->getConnection()->prepare($deleteAvailabilityQuery);
        $dbConn->execute();

        foreach ($intervals as $interval) {
            $availability = new Availability();

                $availability->setUser($user);
                $availability->setWeekDay($day);
                $availability->setStart($interval['start']);
                $availability->setEnd($interval['end']);

            $this->getEntityManager()->persist($availability);

            $this->getEntityManager()->flush();
        }

        return new JsonResponse('SAVED');
    }


}

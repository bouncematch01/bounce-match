<?php

namespace AppBundle\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class BaseController extends Controller
{
    /**
     * @return EloSystemRatin
     */
    protected function getEloSystemRating()
    {
        return $this->get('qubiz_elo_system.rating');
    }

    /**
     * @return Firebase notification Bundle
     */
    protected function getFirebaseNotification()
    {
        return $this->get('qubiz_firebase_notification.notification');
    }

    /**
     * @return Payment Bundle
     */
    protected function getPayment()
    {
        return $this->get('qubiz_mobile_pay.payment');
    }

    /**
     * @return Game Repo
     */
    protected function getGameRepo()
    {
        return $this->get('app.repository.game');
    }

    /**
     * @return UserGame Repo
     */
    protected function getUserGameRepo()
    {
        return $this->get('app.repository.user_game');
    }

    /**
     * @return GameStatus Repo
     */
    protected function getGameStatusRepo()
    {
        return $this->get('app.repository.game_status');
    }

    /**
     * @return User Repo
     */
    protected function getUserRepo()
    {
        return $this->get('app.repository.user');
    }

    /**
     * @return Profile Repo
     */
    protected function getProfileRepo()
    {
        return $this->get('app.repository.profile');
    }

    /**
     * @return Doctrine Entity Manager
     */
    protected function getEntityManager()
    {
        return $this->get('doctrine.orm.entity_manager');
    }

    /**
     * @return ClientManagerInterface
     */
    protected function getOAuthClientManager()
    {
        return $this->get('fos_oauth_server.client_manager');
    }

    /**
     * @return OAuth2
     */
    protected function getOAuthServer()
    {
        return $this->get('qubiz_oauth_server.server');
    }

    /**
     * @return Location Repo
     */
    protected function getLocationRepo()
    {
        return $this->get('app.repository.location');
    }

    /**
     * @return XpLevel Repo
     */
    protected function getXpLevelRepo()
    {
        return $this->get('app.repository.xp_level');
    }

    /**
     * @return Availability Repo
     */
    protected function getAvailabilityRepo()
    {
        return $this->get('app.repository.availability');
    }

    /**
     * @return WeekDay Repo
     */
    protected function getWeekDayRepo()
    {
        return $this->get('app.repository.week_day');
    }

    /**
     * @return Challenge Repo
     */
    protected function getChallengeRepo()
    {
        return $this->get('app.repository.challenge');
    }

    /**
     * @return ChallengeInterval Repo
     */
    protected function getChallengeIntervalRepo()
    {
        return $this->get('app.repository.challenge_interval');
    }

    /**
     * @return ChallengeStatus Repo
     */
    protected function getChallengeStatusRepo()
    {
        return $this->get('app.repository.challenge_status');
    }

    /**
     * @return Country Repo
     */
    protected function getCountryRepo()
    {
        return $this->get('app.repository.country');
    }

    /**
     * @return City Repo
     */
    protected function getCityRepo()
    {
        return $this->get('app.repository.city');
    }

    /**
     * @return Conversation Repo
     */
    protected function getConversationRepo()
    {
        return $this->get('app.repository.conversation');
    }

    /**
     * @return Payment Repo
     */
    protected function getPaymentRepo()
    {
        return $this->get('app.repository.payment');
    }

    /**
     * @return PaymentStatus Repo
     */
    protected function getPaymentStatusRepo()
    {
        return $this->get('app.repository.payment_status');
    }

    /**
     * @return PaymentTransactionStatus Repo
     */
    protected function getPaymentTransactionStatusRepo()
    {
        return $this->get('app.repository.payment_transaction_status');
    }

    /**
     * @return Firebase Controller
     */
    protected function getFirebaseController()
    {
        return $this->get('app.firebase_controller');
    }

    /**
     * @return AppBundle\Service\SwiftEmailManagerService
     */
    protected function getSwiftEmailManagerService()
    {
        return $this->get('app.service.swift_email_manager');
    }
}

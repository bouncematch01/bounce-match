<?php

namespace AppBundle\Controller\API;

use AppBundle\Controller\API\FirebaseController;
use AppBundle\Controller\API\OpponentController;
use AppBundle\Controller\API\VenueController;
use AppBundle\Entity\Challenge;
use AppBundle\Entity\ChallengeInterval;
use AppBundle\Entity\ChallengeStatus;
use AppBundle\Entity\Game;
use AppBundle\Entity\GameStatus;
use AppBundle\Entity\Location;
use AppBundle\Entity\UserGame;
use DateTime;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChallengeController extends BaseController
{

   /**
     *
     * @Post("/challenges")
     *
     * @return JsonResponse
     */
    public function newChallengeAction(Request $request)
    {
        $user      = $this->getUser();
        $opponent  = $this->getUserRepo()->findOneById($request->get('opponent_id'));
        $venue     = $this->getLocationRepo()->findOneById($request->get('venue_id'));
        $intervals = $request->get('intervals');
        $date      = new DateTime(date('Y-m-d', $request->get('date')));

        if (NULL === $venue || NULL === $opponent) {
            return new Response('Wrong info sent.', Response::HTTP_NOT_FOUND);
        }

        if (!isset($intervals)) {
            return new Response('Intervals not received.', Response::HTTP_BAD_REQUEST);
        }

        $existingGameQuery = "SELECT
                                    COUNT(game.id) as gamesNotClosed
                                FROM
                                    game
                                LEFT JOIN user_game as userGame
                                    ON game.id = userGame.game_id
                                WHERE
                                    userGame.user_id = " . $opponent->getId() . " AND
                                    game.status_id IN (" .
                                    GameStatus::IN_PROGRESS .
                                    " , " .
                                    GameStatus::OPENED .
                                    " , " .
                                    GameStatus::UPCOMING_GAME .
                                    ")";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($existingGameQuery);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $openedGamesCount = $result[0]['gamesNotClosed'];

        if ($openedGamesCount > 0) {
            return new Response(
                'Can\'t create a new game. Opponent already has an open game.',
                Response::HTTP_CONFLICT
            );
        }

        $gameStatus      = $this->getGameStatusRepo()->findOneById(GameStatus::CHALLENGE);
        $challengeStatus = $this->getChallengeStatusRepo()->findOneById(ChallengeStatus::NOT_CONFIRMED);

        $game = new Game();
        $game->setLocation($venue);
        $game->setUser($user);
        $game->setStatus($gameStatus);
        $this->getEntityManager()->persist($game);

        $userGame = new UserGame();
        $userGame->setUser($user);
        $userGame->setGame($game);
        $userGame->setScore(0);
        $this->getEntityManager()->persist($userGame);

        $opponentGame = new UserGame();
        $opponentGame->setUser($opponent);
        $opponentGame->setGame($game);
        $opponentGame->setScore(0);
        $this->getEntityManager()->persist($opponentGame);

        $challenge = new Challenge();
        $challenge->setGame($game);
        $challenge->setStatus($challengeStatus);
        $challenge->setDate($date);
        $this->getEntityManager()->persist($challenge);

        foreach($intervals as $interval){
            $challengeInterval = new ChallengeInterval();
            $challengeInterval->setChallenge($challenge);
            $challengeInterval->setStart($interval['start']);
            $challengeInterval->setEnd($interval['end']);
            $this->getEntityManager()->persist($challengeInterval);
        }

        $this->getEntityManager()->flush();

        $this->getFirebaseController()->sendChallengeConfirmationMessage(
            $opponent,
            $challenge->getId(),
            $this->getFirebaseNotification());

        return new JsonResponse('SAVED');
    }

    /**
     *
     * @Get("/users/{id}/challenges")
     *
     * @return JsonResponse
     */
    public function userChallangeAction(Request $request, $id)
    {
        $date     = date('Y-m-d', $request->get('date'));
        $userId   = $this->getUser()->getId();

        $query = "SELECT
                        game.created_at AS gameDate,
                        mygames.game_id AS gameId,
                        mygames.points AS points,
                        mygames.score AS myScore,
                        opponentgames.score AS opponentScore,
                        location.name AS venueName,
                        challenge.id AS challengeId,
                        challenge.date AS challengeDate
                    FROM user_game
                        AS mygames
                    RIGHT JOIN user_game
                        AS opponentgames
                        ON mygames.game_id = opponentgames.game_id AND
                        opponentgames.user_id = " . $id . "
                    LEFT JOIN user
                        AS opponent
                        ON opponent.id = " . $id . "
                    LEFT JOIN profile
                        AS opponentProfile
                        ON opponentProfile.user_id = " . $id . "
                    LEFT JOIN game
                        ON game.id = mygames.game_id
                    LEFT JOIN location
                        ON location.id = game.location_id
                    RIGHT JOIN challenge
                        ON game.id = challenge.game_id
                    WHERE
                            mygames.user_id = " . $userId . "
                        AND
                            game.status_id = " . GameStatus::CHALLENGE . "
                        AND
                            DATE(challenge.date) = '" . $date . "'
                    ORDER BY gameDate DESC;";

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        $challengeList = [];

        foreach ($result as $challenge) {

            $intervalsResponse = $this->forward('AppBundle\Controller\API\ChallengeController::getChallengeIntervalsAction',
                                            [
                                                'request' => $request,
                                                'id'      => $challenge['challengeId']
                                            ]
                                        );

            $challengeList[] = [
                'challenge_id'   => (int) $challenge['challengeId'],
                'game_id'        => (int) $challenge['gameId'],
                'points'         => (int) $challenge['points'],
                'my_score'       => (int) $challenge['myScore'],
                'opponent_score' => (int) $challenge['opponentScore'],
                'date'           => strtotime($challenge['challengeDate']),
                'venue_name'     => $challenge['venueName'],
                'intervals'      => json_decode($intervalsResponse->getContent())
            ];
        }

        return new JsonResponse($challengeList);
    }

    /**
     *
     * @Get("/challenges/{id}/intervals")
     *
     * @return JsonResponse
     */
    public function getChallengeIntervalsAction(Request $request, $id)
    {
        $intervalsQuery = "SELECT start, end FROM challenge_interval WHERE challenge_id = " . $id;
        $dbConn = $this->getEntityManager()->getConnection()->prepare($intervalsQuery);
        $dbConn->execute();
        $intervalResult = $dbConn->fetchAll();

        $intervals = [];
        foreach($intervalResult as $interval){
            $intervals[] = [
                'start' => (int) $interval['start'],
                'end'   => (int) $interval['end']
            ];
        }

        return new JsonResponse($intervals);
    }

    /**
     *
     * @Get("/challenges/outgoing")
     *
     * @return JsonResponse
     */
    public function getOutgoingChallengesAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepo()->findOneByUser($user);

        $myPoints = $profile->getPoints();
        $userId   = $user->getId();

        $query = "SELECT
                        game.id AS gameId,
                        venue.name AS venueName,
                        game.created_at AS gameDate,
                        opponentGame.points AS opponentGamePoints,
                        opponentGame.score AS opponentGameScore,
                        opponentProfile.user_id AS opponentUserId,
                        opponentProfile.id AS opponentId,
                        opponentProfile.first_name AS firstName,
                        opponentProfile.last_name AS lastName,
                        opponentProfile.points AS opponentPoints,
                        opponentProfile.city AS opponentCity,
                        opponentProfile.image_url AS imageUrl,
                        xpLevel.id AS xpLevel,
                        challenge.date AS challengeDate,
                        challenge.id AS challengeId
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            game
                        ON
                            myGame.game_id = game.id AND
                            game.status_id = " . GameStatus::CHALLENGE . " AND
                            game.created_by_user_id = " . $userId . "
                    RIGHT JOIN
                            user_game AS opponentGame
                        ON
                            myGame.game_id = opponentGame.game_id AND
                            opponentGame.user_id != " . $userId . "
                    LEFT JOIN
                            location AS venue
                        ON
                            game.location_id = venue.id
                    LEFT JOIN
                            profile AS opponentProfile
                        ON
                            opponentGame.user_id = opponentProfile.user_id
                    LEFT JOIN
                            user AS opponent
                        ON
                            opponent.id = opponentProfile.user_id
                    RIGHT JOIN
                            xp_level AS xpLevel
                        ON
                            xpLevel.id = opponent.xp_level_id
                    RIGHT JOIN
                            challenge
                        ON
                            challenge.game_id = myGame.game_id
                    WHERE
                            myGame.game_id IN (SELECT game_id FROM user_game WHERE user_id = " . $userId . ")
                        AND
                            myGame.user_id = " . $userId . "
                        AND
                            challenge.date >= CURDATE()
                    ORDER BY
                        game.created_at DESC;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        $challenges = [];

        foreach ($result as $challenge) {
            $gamesNrResponse = $this->forward('AppBundle\Controller\API\GameController::userGamesCountAction',
                                            [
                                                'request' => $request,
                                                'id'      => $challenge['opponentUserId']
                                            ]
                                        );

            $gamesNr = json_decode($gamesNrResponse->getContent())->games_nr;
            $showChanceToWin = (boolean) ($gamesNr >= OpponentController::STILL_BEGGINER_GAMES_COUNT);

            $pointsResults = $this->getEloSystemRating()->getMatchMakingInfo(
                                                            $myPoints,
                                                            $challenge['opponentPoints'],
                                                            $user->getKFactor());

            $chanceToWin = $pointsResults['chanceToWin'];
            $challengeId = $challenge['challengeId'];

            $intervalsQuery = "SELECT start, end FROM challenge_interval WHERE challenge_id = " . $challengeId;
            $dbConn = $this->getEntityManager()->getConnection()->prepare($intervalsQuery);
            $dbConn->execute();
            $intervalResult = $dbConn->fetchAll();

            $overviewInterval = [];
            $intervals        = [];

            if (!empty($intervalResult)) {
                $overviewInterval = [
                    'start' => (int) $intervalResult[0]['start'],
                    'end'   => (int) array_values(array_slice($intervalResult, -1))[0]['end']
                ];

                foreach($intervalResult as $interval){
                    $intervals[] = [
                        'start' => (int) $interval['start'],
                        'end'   => (int) $interval['end']
                    ];
                }
            }

            $challenges[] = [
                'id'                 => (int) $challenge['challengeId'],
                'first_name'         => $challenge['firstName'],
                'last_name'          => $challenge['lastName'],
                'xp_level'           => (int) $challenge['xpLevel'],
                'show_chance_to_win' => $showChanceToWin,
                'chance_to_win'      => (int) $chanceToWin,
                'image_url'          => $challenge['imageUrl'],
                'location'           => $challenge['venueName'],
                'date'               => strtotime($challenge['challengeDate']),
                'intervals'          => $intervals,
                'overview_interval'  => $overviewInterval
            ];
        }

        return new JsonResponse($challenges);
    }

    /**
     *
     * @Get("/challenges/incoming")
     *
     * @return JsonResponse
     */
    public function getIncomingChallengesAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepo()->findOneByUser($user);

        $myPoints = $profile->getPoints();
        $userId   = $user->getId();

        $query = "SELECT
                        game.id AS gameId,
                        venue.name AS venueName,
                        game.created_at AS gameDate,
                        opponentGame.points AS opponentGamePoints,
                        opponentGame.score AS opponentGameScore,
                        opponentProfile.user_id AS opponentUserId,
                        opponentProfile.id AS opponentId,
                        opponentProfile.first_name AS firstName,
                        opponentProfile.last_name AS lastName,
                        opponentProfile.points AS opponentPoints,
                        opponentProfile.city AS opponentCity,
                        opponentProfile.image_url AS imageUrl,
                        xpLevel.id AS xpLevel,
                        challenge.date AS challengeDate,
                        challenge.id AS challengeId
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            game
                        ON
                            myGame.game_id = game.id AND
                            game.status_id = " . GameStatus::CHALLENGE . " AND
                            game.created_by_user_id != " . $userId . "
                    RIGHT JOIN
                            user_game AS opponentGame
                        ON
                            myGame.game_id = opponentGame.game_id AND
                            opponentGame.user_id != " . $userId . "
                    LEFT JOIN
                            location AS venue
                        ON
                            game.location_id = venue.id
                    LEFT JOIN
                            profile AS opponentProfile
                        ON
                            opponentGame.user_id = opponentProfile.user_id
                    LEFT JOIN
                            user AS opponent
                        ON
                            opponent.id = opponentProfile.user_id
                    RIGHT JOIN
                            xp_level AS xpLevel
                        ON
                            xpLevel.id = opponent.xp_level_id
                    RIGHT JOIN
                            challenge
                        ON
                            challenge.game_id = myGame.game_id
                    WHERE
                            myGame.game_id IN (SELECT game_id FROM user_game WHERE user_id = " . $userId . ")
                        AND
                            myGame.user_id = " . $userId . "
                        AND
                            challenge.date >= CURDATE()
                    ORDER BY
                        challenge.date DESC;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        $challenges = [];

        foreach ($result as $challenge) {
            $gamesNrResponse = $this->forward('AppBundle\Controller\API\GameController::userGamesCountAction',
                                            [
                                                'request' => $request,
                                                'id'      => $challenge['opponentUserId']
                                            ]
                                        );

            $gamesNr = json_decode($gamesNrResponse->getContent())->games_nr;
            $showChanceToWin = (boolean) ($gamesNr >= OpponentController::STILL_BEGGINER_GAMES_COUNT);

            $pointsResults = $this->getEloSystemRating()->getMatchMakingInfo(
                                                            $myPoints,
                                                            $challenge['opponentPoints'],
                                                            $user->getKFactor());

            $chanceToWin = $pointsResults['chanceToWin'];
            $challengeId = $challenge['challengeId'];

            $intervalsQuery = "SELECT start, end FROM challenge_interval WHERE challenge_id = " . $challengeId;
            $dbConn = $this->getEntityManager()->getConnection()->prepare($intervalsQuery);
            $dbConn->execute();
            $intervalResult = $dbConn->fetchAll();

            $intervals = [];
            foreach($intervalResult as $interval){
                $intervals[] = [
                    'start' => (int) $interval['start'],
                    'end'   => (int) $interval['end']
                ];
            }

            $challenges[] = [
                'id'                 => (int) $challenge['challengeId'],
                'first_name'         => $challenge['firstName'],
                'last_name'          => $challenge['lastName'],
                'xp_level'           => (int) $challenge['xpLevel'],
                'show_chance_to_win' => $showChanceToWin,
                'chance_to_win'      => (int) $chanceToWin,
                'image_url'          => $challenge['imageUrl'],
                'location'           => $challenge['venueName'],
                'date'               => strtotime($challenge['challengeDate']),
                'intervals'          => $intervals
            ];
        }

        return new JsonResponse($challenges);
    }

    /**
     *
     * @Get("/challenges/incoming-nr")
     *
     * @return JsonResponse
     */
    public function getIncomingChallengesNrAction()
    {

        $userId = $this->getUser()->getId();

        $query = "SELECT
                        game.id AS gameId
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            game
                        ON
                            myGame.game_id = game.id AND
                            game.status_id = " . GameStatus::CHALLENGE . " AND
                            game.created_by_user_id != " . $userId . "
                    RIGHT JOIN
                            challenge
                        ON
                            challenge.game_id = myGame.game_id
                    WHERE
                            myGame.game_id IN (
                                SELECT
                                    game_id
                                FROM
                                    user_game
                                WHERE
                                    user_id = " . $userId . ")
                        AND
                            myGame.user_id = " . $userId . "
                        AND
                            challenge.date >= CURDATE()
                    GROUP BY
                        gameId; ";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        $challengesNr = [
            'challenges_nr' => count($result)
        ];

        return new JsonResponse($challengesNr);
    }

    /**
     *
     * @Get("/challenges/outgoing-nr")
     *
     * @return JsonResponse
     */
    public function getOutgoingChallengesNrAction()
    {

        $userId = $this->getUser()->getId();

        $query = "SELECT
                        game.id AS gameId
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            game
                        ON
                                myGame.game_id = game.id
                            AND
                                game.status_id = " . GameStatus::CHALLENGE . "
                            AND
                                game.created_by_user_id = " . $userId . "
                    RIGHT JOIN
                            challenge
                        ON
                            challenge.game_id = myGame.game_id
                    WHERE
                            myGame.game_id IN (
                                SELECT
                                    game_id
                                FROM
                                    user_game
                                WHERE
                                    user_id = " . $userId . ")
                        AND
                            myGame.user_id = " . $userId . "
                        AND
                            challenge.date >= CURDATE()
                    GROUP BY
                        gameId; ";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        $challengesNr = [
            'challenges_nr' => count($result)
        ];

        return new JsonResponse($challengesNr);
    }

    /**
     *
     * @Patch("/challenges/{id}/accept")
     *
     * @return JsonResponse
     */
    public function acceptChallengeAction(Request $request, $id)
    {
        $user   = $this->getUser();
        $userId = $user->getId();

        $acceptedHour = $request->get('accepted_hour');

        if (!isset($acceptedHour)) {
            return new Response('Accepted hour not received.', Response::HTTP_BAD_REQUEST);
        }

        $challenge = $this->getChallengeRepo()->findOneById($id);

        if (NULL === $challenge) {
            return new Response('Wrong challenge sent.', Response::HTTP_NOT_FOUND);
        }

        $dayId = date('N', strtotime($challenge->getDate()->format('Y-m-d')));
        $hourAvailable = VenueController::isHourAvailable($dayId, $acceptedHour);

        if (!$hourAvailable) {
            return new Response('The court is already booked for this hour.', Response::HTTP_CONFLICT);
        }

        $challengeStatus = $this->getChallengeStatusRepo()->findOneById(ChallengeStatus::ACCEPTED);
        $gameStatus      = $this->getGameStatusRepo()->findOneById(GameStatus::UPCOMING_GAME);

        $challenge->setHour($acceptedHour);
        $challenge->setStatus($challengeStatus);

        $game = $challenge->getGame();
        $game->setStatus($gameStatus);

        VenueController::bookCourt($challenge->getDate()->format('Y-m-d'), $acceptedHour);

        $this->getEntityManager()->flush();

        $userGames = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $opponent   = null;
        $opponentId = null;

        foreach($userGames as $userGame) {
            $player   = $userGame->getUser();
            $playerId = $player->getId();

            if ($userId !== $playerId) {
                $opponent   = $player;
                $opponentId = $playerId;
            }
        }

        self::sendChallengesRemovedNotifications($userId, $opponentId);
        self::changeChallengeGamesStatus($userId, $opponentId);
        self::changeChallengesStatus($userId, $opponentId);

        $response = [
            'status' => $challengeStatus->getName()
        ];

        $this->getFirebaseController()->sendChallengeAcceptedMessage(
            $opponent,
            $challenge->getId(),
            $this->getFirebaseNotification());

        $profile = $this->getProfileRepo()->findOneByUser($opponent);
        $this->getSwiftEmailManagerService()->sendReservationEmail(
            $acceptedHour,
            $challenge->getDate()->format('Y-m-d'),
            $profile->getFirstName() . ' ' .$profile->getLastName()
        );

        return new JsonResponse($response);
    }

    /**
     *
     * @Patch("/challenges/{id}/reject")
     *
     * @return JsonResponse
     */
    public function rejectChallengeAction(Request $request, $id)
    {
        $user   = $this->getUser();
        $userId = $user->getId();

        $challenge = $this->getChallengeRepo()->findOneById($id);

        if (NULL === $challenge) {
            return new Response('Wrong challenge sent.', Response::HTTP_NOT_FOUND);
        }

        $challengeStatus = $this->getChallengeStatusRepo()->findOneById(ChallengeStatus::REJECTED);
        $gameStatus      = $this->getGameStatusRepo()->findOneById(GameStatus::DELETED);

        $challenge->setStatus($challengeStatus);

        $game = $challenge->getGame();
        $game->setStatus($gameStatus);

        $this->getEntityManager()->flush();

        $userGames = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $opponent   = null;
        $opponentId = null;

        foreach($userGames as $userGame) {
            $player   = $userGame->getUser();
            $playerId = $player->getId();

            if ($userId !== $playerId) {
                $opponent = $player;
            }
        }

        $response = [
            'status' => $challengeStatus->getName()
        ];

        $this->getFirebaseController()->sendUserChallengeRejectedMessage(
            $opponent,
            $challenge->getId(),
            $this->getFirebaseNotification());

        return new JsonResponse($response);
    }

    /**
     *
     * @Delete("/challenges")
     *
     * @return JsonResponse
     */
    public function deleteChallengesAction()
    {
        $userId = $this->getUser()->getId();

        $query = "SELECT
                        user_game.user_id AS userId,
                        challenge.id AS challengeId,
                        user_game.game_id AS gameId,
                        user.firebase_token AS firebaseToken,
                        user.device_type AS deviceType
                    FROM
                        user_game
                    JOIN
                            challenge
                        ON
                            user_game.game_id = challenge.game_id
                    JOIN
                            game
                        ON
                            challenge.game_id = game.id AND
                            game.status_id = " . GameStatus::CHALLENGE . "
                    JOIN
                            user
                        ON
                            user_game.user_id = user.id
                    WHERE
                            user_id != " . $userId . "
                        AND
                            game.created_by_user_id = " . $userId . "
                        AND
                            challenge.status_id = " . ChallengeStatus::NOT_CONFIRMED . ";";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $challengesResponse = $dbConn->fetchAll();

        if (empty($challengesResponse)) {
            $response = [
                'deleted' => true
            ];

            return new JsonResponse($response);
        }

        $challengesId = [];
        $gamesId      = [];

        foreach($challengesResponse as $challengeResponse) {
            $challengesId[] = $challengeResponse['challengeId'];
            $gamesId[]      = $challengeResponse['gameId'];
        }

        $challengesIdFilterQuery = implode(',', $challengesId);
        $gamesIdFilterQuery      = implode(',', $gamesId);

        $query = "UPDATE
                        game
                    SET
                        status_id = " . GameStatus::DELETED . "
                    WHERE
                        id IN (" . $gamesIdFilterQuery . ");";
        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();


        $query = "UPDATE
                        challenge
                    SET
                        status_id = " . ChallengeStatus::DELETED . "
                    WHERE
                        id IN (" . $challengesIdFilterQuery . ");";
        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();

        foreach($challengesResponse as $challengeResponse) {
            $this->getFirebaseController()->sendChallengeRejectedMessage(
                $challengeResponse['firebaseToken'],
                $challengeResponse['deviceType'],
                $challengeResponse['challengeId'],
                $this->getFirebaseNotification());
        }

        $response = [
            'deleted' => true
        ];

        return new JsonResponse($response);
    }

    /**
     *
     * @Delete("/allchallenges")
     *
     * @return JsonResponse
     */
    public function deleteAllChallengesAction()
    {
        $query = "DELETE FROM challenge_interval;";
        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();

        $query = "DELETE FROM challenge;";
        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();

        $query = "UPDATE game SET status_id = " . GameStatus::CLOSED . ";";
        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();

        $response = [
            'deleted' => true
        ];

        return new JsonResponse($response);
    }

    private function changeChallengeGamesStatus(
        $userId,
        $opponentId = null,
        $status = GameStatus::CHALLENGE,
        $newStatus = GameStatus::DELETED
    ){
        $gamesQuery = self::allGamesQuery($userId, $opponentId);

        $query = "UPDATE
                        game
                    SET
                        status_id = " . $newStatus . "
                    WHERE
                        id IN (" . $gamesQuery . ")
                    AND
                        status_id = " . $status . ";
                    ";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
    }

    private function changeChallengesStatus(
        $userId,
        $opponentId = null,
        $status = ChallengeStatus::NOT_CONFIRMED,
        $newStatus = ChallengeStatus::DELETED
    ){
        $gamesQuery = self::allGamesQuery($userId, $opponentId);

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($gamesQuery);
        $dbConn->execute();
        $games = $dbConn->fetchAll();

        $challengeGamesQuery = '';

        if (!empty($games)) {
            $challengeGamesQuery = " AND game_id IN ( ";

            foreach($games as $game) {
                $challengeGamesQuery .= $game['id'];
                if ($game !== end($games)) {
                    $challengeGamesQuery .= ", ";
                } else {
                    $challengeGamesQuery .= ")";
                }
            }
        }

        $query = "UPDATE
                        challenge
                    SET
                        status_id = " . $newStatus . "
                    WHERE
                        status_id = " . $status . $challengeGamesQuery . ";
                    ";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
    }

    private function allGamesQuery($userId, $opponentId = null)
    {
        $userQuery = " = " . $userId;

        if (null !== $opponentId) {
            $userQuery = " IN ( " . $userId . ", " . $opponentId . " ) ";
        }

        $query = "SELECT
                        myGame.game_id AS id
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            challenge
                        ON
                            challenge.game_id = myGame.game_id
                    WHERE
                        myGame.game_id IN (SELECT game_id FROM user_game WHERE user_id " . $userQuery . ")
                        AND myGame.user_id " . $userQuery . "
                    GROUP BY id ";

        return $query;
    }

    private function sendChallengesRemovedNotifications($userId, $opponentId)
    {
        $userQuery = $userId . ", " . $opponentId;

        $query = "SELECT
                        myGame.user_id AS userId,
                        user.firebase_token AS firebaseToken,
                        user.device_type AS deviceType,
                        challenge.id AS challengeId
                    FROM
                        user_game AS myGame
                    JOIN
                            challenge
                        ON
                            challenge.game_id = myGame.game_id
                    JOIN
                            user
                        ON
                            user.id = myGame.user_id
                    WHERE
                        myGame.game_id IN (SELECT
                                                game_id
                                            FROM
                                                user_game
                                            WHERE
                                                user_id
                                            IN
                                                ( " . $userQuery . " ) )
                        AND
                            myGame.user_id  NOT IN ( " . $userQuery . " )
                        AND
                            challenge.status_id = " . ChallengeStatus::NOT_CONFIRMED . "
                    GROUP BY
                        userId, challengeId;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $usersWithChallenges = $dbConn->fetchAll();

        foreach ($usersWithChallenges as $userWithChallenge) {
            $this->getFirebaseController()->sendChallengeRemovedMessage(
                    $userWithChallenge['firebaseToken'],
                    $userWithChallenge['deviceType'],
                    $userWithChallenge['challengeId'],
                    $userId,
                    $opponentId,
                    $this->getFirebaseNotification()
                );
        }

        return true;
    }
}

<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\Conversation;
use AppBundle\Entity\Game;
use AppBundle\Entity\GameStatus;
use AppBundle\Entity\User;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ConversationController extends BaseController
{
    /**
     *
     * @Get("/conversation")
     *
     * @return JsonResponse
     */
    public function getConversationAction()
    {
        $userId = $this->getUser()->getId();

        $inProgressGame = $this->getConversationGame($userId);

        if (null === $inProgressGame)
        {
            return new JsonResponse([]);
        }

        $conversation = $this->getConversationRepo()->findByGame($inProgressGame, array('id' => 'DESC'));

        $conversationResponse = [];

        if (empty($conversation)){
            return new JsonResponse($conversationResponse);
        }

        $gameUsers = $this
                ->getUserGameRepo()
                ->findByGame($inProgressGame);

        $opponent   = null;

        foreach($gameUsers as $gameUser) {
            $player   = $gameUser->getUser();
            $playerId = $player->getId();

            if ($userId !== $playerId) {
                $opponent   = $player;
                $opponentId = $playerId;
            }
        }

        $this->updateMessageStatus($inProgressGame->getId(), $opponentId);

        foreach ($conversation as $message){
            $conversationResponse[] = [
                'user_id' => $message->getUser()->getId(),
                'message' => utf8_decode($message->getMessage())
            ];
        }

        return new JsonResponse($conversationResponse);
    }

    /**
     *
     * @Get("/conversation/opponent")
     *
     * @return JsonResponse
     */
    public function getConversationOpponentAction()
    {
        $userId = $this->getUser()->getId();

        $inProgressGame = $this->getConversationGame($userId);

        if (null === $inProgressGame)
        {
            return new JsonResponse([]);
        }

        $gameUsers = $this
                ->getUserGameRepo()
                ->findByGame($inProgressGame);

        $opponent   = null;

        foreach($gameUsers as $gameUser) {
            $player   = $gameUser->getUser();
            $playerId = $player->getId();

            if ($userId !== $playerId) {
                $opponent   = $player;
                $opponentId = $playerId;
            }
        }

        $profile = $this->getProfileRepo()->findOneByUser($opponent);



        $profile = [
            'name'      => $profile->getFirstName() . ' ' . $profile->getLastName(),
            'image_url' => $profile->getImageUrl()
        ];

        return new JsonResponse($profile);
    }

   /**
     *
     * @Post("/conversation")
     *
     * @return JsonResponse
     */
    public function newConversationCommentAction(Request $request)
    {
        $user = $this->getUser();
        $userId = $user->getId();

        $inProgressGame = $this->getConversationGame($userId);

        if (null === $inProgressGame)
        {
            return new JsonResponse([]);
        }

        if (null === $inProgressGame) {
            return new Response('No active games.', Response::HTTP_NOT_FOUND);
        }

        $message = utf8_encode($request->get('message'));

        $dayId = $request->get('day');
        $day = $this->getWeekDayRepo()->findOneById($dayId);

        $conversation = new Conversation();

        $conversation->setUser($user);
        $conversation->setGame($inProgressGame);
        $conversation->setMessage($message);

        $this->getEntityManager()->persist($conversation);
        $this->getEntityManager()->flush();

        $gameUsers = $this
                    ->getUserGameRepo()
                    ->findByGame($inProgressGame);

        $opponent   = null;

        foreach($gameUsers as $gameUser) {
            $player   = $gameUser->getUser();
            $playerId = $player->getId();

            if ($userId !== $playerId) {
                $opponent   = $player;
                $opponentId = $playerId;
            }
        }

        $unreadMessagesNr = $this->getUnreadMessagesNr($inProgressGame->getId(), $userId);

        $this->getFirebaseController()->sendConversationUpdatedMessage(
            $opponent,
            $unreadMessagesNr,
            $this->getFirebaseNotification());

        return new JsonResponse('SAVED');
    }

    /**
     *
     * @Get("/conversation/unread_messages_nr")
     *
     * @return JsonResponse
     */
    public function getConversationUnreadMessagesNrAction()
    {
        $userId = $this->getUser()->getId();

        $inProgressGame = $this->getConversationGame($userId);

        if (null === $inProgressGame)
        {
            return new JsonResponse([]);
        }

        $gameUsers = $this
                ->getUserGameRepo()
                ->findByGame($inProgressGame);

        $opponent   = null;

        foreach($gameUsers as $gameUser) {
            $player   = $gameUser->getUser();
            $playerId = $player->getId();

            if ($userId !== $playerId) {
                $opponent   = $player;
                $opponentId = $playerId;
            }
        }

        $unreadMessagesNr = $this->getUnreadMessagesNr($inProgressGame->getId(), $opponentId);

        $response = [
            'unread_messages_nr' => $unreadMessagesNr
        ];

        return new JsonResponse($response);
    }

    public function getConversationGame($userId)
    {
        $query = "SELECT
                        game.id AS gameId
                    FROM
                        game
                    RIGHT JOIN
                            user_game
                        ON
                            user_game.game_id = game.id
                    WHERE
                            user_game.user_id = " . $userId . "
                        AND
                            game.status_id IN (" . GameStatus::UPCOMING_GAME . "," . GameStatus::NOT_CONFIRMED . ");";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        if (empty($result)) {
            return null;
        }

        $inProgressGameId = $result[0]['gameId'];

        return $this->getGameRepo()->findOneById($inProgressGameId);
    }

    public function updateMessageStatus($gameId, $opponentId)
    {
        $query = "UPDATE
                        conversation
                    SET
                        read_status = " . Conversation::READ_STATUS . "
                    WHERE
                        user_id = " . $opponentId . " AND
                        game_id = " . $gameId . " AND
                        read_status = " . Conversation::NOT_READ_STATUS . ";";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
    }

    public function getUnreadMessagesNr($gameId, $opponentId)
    {
        $query = "SELECT
                        COUNT(id) AS messagesNr
                    FROM
                        conversation
                    WHERE
                        user_id = " . $opponentId . " AND
                        game_id = " . $gameId . " AND
                        read_status = " . Conversation::NOT_READ_STATUS . ";";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        return $result[0]['messagesNr'];
    }
}

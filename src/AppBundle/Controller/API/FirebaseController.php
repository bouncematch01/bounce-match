<?php

namespace AppBundle\Controller\API;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FirebaseController extends BaseController
{
    public function sendGameConfirmationMessage($user, $gameId, $firebase)
    {
        $message = [
            'title'             => '1on1 score confirmation message',
            'subtitle'          => '1on1 score confirmation message',
            'body'              => '1on1 match score was updated. Please confirm!',
            'notification_type' => 'game_confirmation',
            'click_action'      => 'customUIActionIos'
        ];

        $extraMessage = [
            'game_id' => $gameId
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendGameClosedMessage($user, $gameId, $firebase)
    {
        $message = [
            'title'             => '1on1 Game closed',
            'subtitle'          => '1on1 Game closed',
            'body'              => '1on1 Game closed!',
            'notification_type' => 'game_closed',
            'click_action'      => 'customUIClosed'
        ];

        $extraMessage = [
            'game_id' => $gameId
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendChallengeConfirmationMessage($user, $challengeId, $firebase)
    {
        $message = [
            'title'             => '1on1 challenge confirmation',
            'subtitle'          => '1on1 challenge confirmation',
            'body'              => '1on1 challenge confirmation!',
            'notification_type' => 'challenge_confirmation',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'challenge_id' => $challengeId,
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendChallengeAcceptedMessage($user, $challengeId, $firebase)
    {
        $message = [
            'title'             => '1on1 challenge',
            'subtitle'          => '1on1 challenge accepted message',
            'body'              => '1on1 challenge accepted!',
            'notification_type' => 'challenge_accepted',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'challenge_id' => $challengeId,
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendChallengeRejectedMessage($firebaseToken = null, $deviceType = null, $challengeId, $firebase)
    {
        $message = [
            'title'             => '1on1 challenge',
            'subtitle'          => '1on1 challenge rejected message',
            'body'              => '1on1 challenge rejected!',
            'notification_type' => 'challenge_cancelled',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'challenge_id' => $challengeId,
        ];

        return $firebase->sendNotification(
            $firebaseToken,
            $deviceType,
            $message,
            $extraMessage
        );
    }

    public function sendUserChallengeRejectedMessage($user, $challengeId, $firebase)
    {
        $firebaseToken = $user->getFirebaseToken();
        $deviceType    = $user->getDeviceType();

        return self::sendChallengeRejectedMessage($firebaseToken, $deviceType, $challengeId, $firebase);
    }

    public function sendValidateScoreMessage($user, $gameId, $firebase)
    {
        $message = [
            'title'             => '1on1 challenge',
            'subtitle'          => '1on1 validate score message',
            'body'              => '1on1 validate score!',
            'notification_type' => 'confirm_score',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'game_id' => $gameId
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendGameRejectedMessage($user, $gameId, $message, $firebase)
    {
        $message = [
            'title'             => '1on1 game',
            'subtitle'          => '1on1 score rejected message',
            'body'              => $message['reason'] . ' ' . $message['wrong_score'],
            'notification_type' => 'score_rejected',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'game_id' => $gameId
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendGameCanceledMessage($user, $gameId, $message, $firebase)
    {
        $message = [
            'title'             => '1on1 game',
            'subtitle'          => '1on1 game canceled message',
            'body'              => $message['reason'] . ' ' . $message['personal_reason'],
            'notification_type' => 'game_cancelled',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'game_id' => $gameId
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendScoreConfirmedMessage($user, $gameId, $firebase)
    {
        $message = [
            'title'             => '1on1 game',
            'subtitle'          => '1on1 score accepted message',
            'body'              => '1on1 score accepted message',
            'notification_type' => 'score_confirmed',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'game_id' => $gameId
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendConversationUpdatedMessage($user, $unreadMessagesNr, $firebase)
    {
        $message = [
            'title'             => '1on1 conversation',
            'subtitle'          => '1on1 conversation message received',
            'body'              => '1on1 conversation message received',
            'notification_type' => 'conversation_message',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'unread_messages_nr' => $unreadMessagesNr
        ];

        return $firebase->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function sendChallengeRemovedMessage(
        $firebaseToken = null,
        $deviceType = null,
        $challengeId,
        $userId,
        $opponentId,
        $firebase)
    {
        $message = [
            'title'             => '1on1 challenge',
            'subtitle'          => '1on1 challenge and user removed message',
            'body'              => '1on1 challenge and user removed!',
            'notification_type' => 'challenge_and_user_removed',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'challenge_id'      => $challengeId,
            'user_id'           => $userId,
            'opponent_id'       => $opponentId
        ];

        return $firebase->sendNotification(
            $firebaseToken,
            $deviceType,
            $message,
            $extraMessage
        );
    }
}

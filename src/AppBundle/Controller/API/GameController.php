<?php

namespace AppBundle\Controller\API;

use AppBundle\Controller\API\FirebaseController;
use AppBundle\Controller\API\OpponentController;
use AppBundle\Controller\API\VenueController;
use AppBundle\Controller\API\PaymentController;
use AppBundle\Entity\ChallengeStatus;
use AppBundle\Entity\Conversation;
use AppBundle\Entity\Game;
use AppBundle\Entity\GameStatus;
use AppBundle\Entity\PaymentStatus;
use AppBundle\Entity\PaymentTransactionStatus;
use AppBundle\Entity\UserGame;
use DateTime;
use DateTimeZone;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GameController extends BaseController
{
    const FREE_GAME_CANCELATION_HOURS = 24;

    /**
     *
     * @Get("/users/{id}/games")
     *
     * @return JsonResponse
     */
    public function gameHistoryAction(Request $request, $id)
    {
        $userId   = $this->getUser()->getId();

        $query = "SELECT
                        game.created_at AS gameDate,
                        mygames.game_id AS gameId,
                        mygames.points AS points,
                        mygames.score AS myScore,
                        opponentgames.score AS opponentScore,
                        location.name AS locationName
                    FROM user_game
                        AS mygames
                    RIGHT JOIN user_game
                        AS opponentgames
                        ON mygames.game_id = opponentgames.game_id AND
                        opponentgames.user_id = " . $id . "
                    LEFT JOIN user
                        AS opponent
                        ON opponent.id = " . $id . "
                    LEFT JOIN profile
                        AS opponentProfile
                        ON opponentProfile.user_id = " . $id . "
                    LEFT JOIN game
                        ON game.id = mygames.game_id
                    LEFT JOIN location
                        ON location.id = game.location_id
                    WHERE mygames.user_id = " . $userId . "
                    ORDER BY gameDate DESC;";

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        $gameList = [];

        foreach ($result as $game) {
            $gameList[] = [
                'game_id'        => (int) $game['gameId'],
                'points'         => (int) $game['points'],
                'my_score'       => (int) $game['myScore'],
                'opponent_score' => (int) $game['opponentScore'],
                'city'           => $game['locationName'],
                'date'           => strtotime($game['gameDate'])
            ];
        }

        return new JsonResponse($gameList);
    }

    /**
     *
     * @Get("/users/{id}/games-count")
     *
     * @return JsonResponse
     */
    public function userGamesCountAction(Request $request, $id)
    {
        $user = $this->getUserRepo()->findOneById($id);

        $query = "SELECT
                        COUNT(user_game.id) AS userGames
                    FROM
                        user_game
                    RIGHT JOIN
                            game
                        ON
                            user_game.game_id = game.id
                    WHERE
                        user_game.user_id = " . $user->getId() . " AND
                        game.status_id = '" . GameStatus::CLOSED . "';";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $games = $result[0]['userGames'];

        return new JsonResponse([
            'games_nr' => (int) $games
        ]);
    }

   /**
     *
     * @Get("/games/{id}", requirements={"id" = "\d+"})
     *
     * @return JsonResponse
     */
    public function gameAction(Request $request, $id)
    {
        $gameQuery = "SELECT
                            game.id as gameId,
                            location.name as locationName,
                            game_status.name as gameStatus
                        FROM
                            game
                        LEFT JOIN location
                            ON game.location_id = location.id
                        LEFT JOIN game_status
                            ON game.status_id = game_status.id
                        WHERE
                            game.id = " . $id;

        $dbConn = $this->getEntityManager()->getConnection()->prepare($gameQuery);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        if (!isset($result[0]['gameId'])) {
            return new Response('Game not found.', Response::HTTP_NOT_FOUND);
        }

        $game = [];
        $game['game_id']       = (int) $result[0]['gameId'];
        $game['location_name'] = $result[0]['locationName'];
        $game['game_status']   = $result[0]['gameStatus'];

        $profileQuery = "SELECT
                                userGame.id AS userGameId,
                                userGame.user_id AS userId,
                                userGame.score AS score,
                                userGame.points AS points,
                                profile.id AS profileId,
                                profile.first_name AS firstName,
                                profile.last_name AS lastName,
                                profile.image_url AS profileImage,
                                profile.city AS profileCity,
                                profile.points AS profilePoints
                            FROM
                                user_game AS userGame
                            LEFT JOIN profile
                                ON userGame.user_id = profile.user_id
                            WHERE
                                userGame.game_id = " . $id;

        $dbConn = $this->getEntityManager()->getConnection()->prepare($profileQuery);
        $dbConn->execute();
        $profiles = $dbConn->fetchAll();

        $gameProfiles = [];

        foreach ($profiles as $profile) {
            $gameProfiles[] = [
                'id'      => (int) $profile['userId'],
                'profile' => [
                    'id'         => (int) $profile['profileId'],
                    'first_name' => $profile['firstName'],
                    'last_name'  => $profile['lastName'],
                    'image_url'  => $profile['profileImage'],
                    'city'       => $profile['profileCity'],
                    'points'     => (int) $profile['profilePoints'],
                    'game_info'  => [
                        'score'  => (int) $profile['score'],
                        'points' => (int) $profile['points']
                    ]
                ]
            ];
        }

        $game['user'] = $gameProfiles;

        return new JsonResponse($game);
    }

    /**
     *
     * @Get("/games/opened")
     *
     * @return JsonResponse
     */
    public function gamesOpenedAction(Request $request)
    {
        $userId = $this->getUser()->getId();

        $query = "SELECT
                        game.id AS gameId,
                        mygames.user_id AS opponentId
                    FROM user_game
                        AS mygames
                    RIGHT JOIN game
                        ON mygames.game_id = game.id AND
                        game.created_by_user_id != " . $userId . "
                    WHERE mygames.user_id = " . $userId . " AND
                          game.status_id = '" . GameStatus::OPENED . "'
                    ORDER BY game.created_at DESC;";

        $smtp = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $smtp->execute();
        $result = $smtp->fetchAll();

        $gameList = [];

        foreach ($result as $game) {
            $gameList[] = ['game_id' => (int) $game['gameId']];
        }

        return new JsonResponse($gameList);
    }

    /**
     *
     * @Get("/games/won")
     *
     * @return JsonResponse
     */
    public function gamesWonAction(Request $request)
    {
        $userId = $this->getUser()->getId();

        $query = "SELECT
                        game.id AS gameId,
                        mygames.user_id AS opponentId
                    FROM user_game
                        AS mygames
                    RIGHT JOIN game
                        ON mygames.game_id = game.id
                    WHERE mygames.user_id = " . $userId . " AND
                          game.status_id = '" . GameStatus::CLOSED . "' AND
                          mygames.score = 3
                    GROUP BY gameId;";

        $smtp = $this->getEntityManager()->getConnection()->prepare($query);
        $smtp->execute();
        $result = $smtp->fetchAll();

        $gamesWon = [
            'games_won' => count($result)
        ];

        return new JsonResponse($gamesWon);
    }

    /**
     *
     * @Get("/games/closed-nr")
     *
     * @return JsonResponse
     */
    public function gamesClosedNrAction(Request $request)
    {
        $user = $this->getUser();

        $query = "SELECT
                        COUNT(user_game.id) AS userGames
                    FROM
                        user_game
                    RIGHT JOIN
                            game
                        ON
                            user_game.game_id = game.id
                    WHERE
                        user_game.user_id = " . $user->getId() . " AND
                        game.status_id = '" . GameStatus::CLOSED . "';";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $games = $result[0]['userGames'];

        return new JsonResponse([
            'games_nr' => (int) $games
        ]);
    }

    /**
     *
     * @Get("/games/upcoming")
     *
     * @return JsonResponse
     */
    public function getUpcomingGameAction(Request $request)
    {
        $user    = $this->getUser();
        $userId  = $user->getId();
        $profile = $this->getProfileRepo()->findOneByUser($user);
        $points  = $profile->getPoints();

        $query = "SELECT
                        game.id as gameId,
                        myGame.points as myPoints,
                        myGame.score as myScore,
                        venue.name as venueName,
                        game.created_at as gameDate,
                        opponentGame.points as opponentGamePoints,
                        opponentGame.score as opponentGameScore,
                        opponent.user_id as opponentUserId,
                        opponent.id as opponentProfileId,
                        opponent.first_name as opponentFirstName,
                        opponent.last_name as opponentLastName,
                        opponent.points as opponentPoints,
                        opponent.city AS opponentCity,
                        opponent.image_url as opponentImageUrl,
                        challenge.date as challengeDate,
                        challenge.hour as challengeHour,
                        xp_level.name AS xpLevel,
                        xp_level.id AS xpLevelId
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            game
                        ON
                            myGame.game_id = game.id AND
                            game.status_id = '" . GameStatus::UPCOMING_GAME . "'
                    RIGHT JOIN
                            challenge
                        ON
                            myGame.game_id = challenge.game_id
                    RIGHT JOIN
                            user_game AS opponentGame
                        ON
                            myGame.game_id = opponentGame.game_id AND
                            opponentGame.user_id != " . $userId . "
                    LEFT JOIN
                            location AS venue
                        ON
                            game.location_id = venue.id
                    LEFT JOIN
                            profile AS opponent
                        ON
                            opponentGame.user_id = opponent.user_id
                    RIGHT JOIN
                            user
                        ON
                            myGame.user_id = user.id
                    RIGHT JOIN
                            xp_level
                        ON
                            user.xp_level_id = xp_level.id
                    WHERE
                        myGame.game_id IN (SELECT game_id FROM user_game WHERE user_id = " . $userId . ")
                        AND myGame.user_id = " . $userId . "
                    ORDER BY
                        game.created_at DESC;";

        $smtp = $this->getEntityManager()->getConnection()->prepare($query);
        $smtp->execute();
        $result = $smtp->fetchAll();

        $upcomingGameResponse = [];

        if (isset($result[0])) {
            $upcomingGame = $result[0];

            $gamesNrResponse = $this->forward('AppBundle\Controller\API\GameController::userGamesCountAction',
                                            [
                                                'request' => $request,
                                                'id'      => $upcomingGame['opponentUserId']
                                            ]
                                        );

            $gamesNr = json_decode($gamesNrResponse->getContent())->games_nr;
            $showChanceToWin = (boolean) ($gamesNr >= OpponentController::STILL_BEGGINER_GAMES_COUNT);

            $pointsResults = $this->getEloSystemRating()->getMatchMakingInfo(
                                                            $points,
                                                            $upcomingGame['opponentPoints'],
                                                            $user->getKFactor());
            $chanceToWin = $pointsResults['chanceToWin'];

            $gameDate = new DateTime($upcomingGame['challengeDate'], new DateTimeZone('Europe/Bucharest'));
            $gameHour = $upcomingGame['challengeHour'];

            $minutes = round(60 * ($gameHour - floor($gameHour)));
            $hour    = floor($gameHour);

            date_time_set($gameDate, $hour, $minutes);

            $unreadMessagesNr = $this->getUnreadMessagesNr(
                                            $upcomingGame['gameId'],
                                            $upcomingGame['opponentUserId']
                                        );

            $upcomingGameResponse = [
                'id'                 => (int) $upcomingGame['gameId'],
                'chance_to_win'      => (int) $chanceToWin,
                'venue_name'         => $upcomingGame['venueName'],
                'game_date'          => $gameDate->getTimeStamp(),
                'xp_level'           => (int) $upcomingGame['xpLevelId'],
                'show_chance_to_win' => $showChanceToWin,
                'unread_messages_nr' => (int) $unreadMessagesNr,
                'profile'       => [
                    'id'         => $upcomingGame['opponentProfileId'],
                    'user_id'    => $upcomingGame['opponentUserId'],
                    'first_name' => $upcomingGame['opponentFirstName'],
                    'last_name'  => $upcomingGame['opponentLastName'],
                    'points'     => $upcomingGame['opponentPoints'],
                    'image_url'  => $upcomingGame['opponentImageUrl']
                ]
            ];
        }

        return new JsonResponse($upcomingGameResponse);
    }

    /**
     *
     * @Get("/games/not-confirmed")
     *
     * @return JsonResponse
     */
    public function getNotConfirmedGameAction(Request $request)
    {
        $user    = $this->getUser();
        $userId  = $user->getId();
        $profile = $this->getProfileRepo()->findOneByUser($user);
        $points  = $profile->getPoints();

        $query = "SELECT
                        game.id as gameId,
                        game.score_inserted_by_user_id as scoreInsertedByUserId,
                        myGame.points as myPoints,
                        myGame.score as myScore,
                        venue.name as venueName,
                        game.created_at as gameDate,
                        opponentGame.points as opponentGamePoints,
                        opponentGame.score as opponentGameScore,
                        opponent.user_id as opponentUserId,
                        opponent.id as opponentProfileId,
                        opponent.first_name as opponentFirstName,
                        opponent.last_name as opponentLastName,
                        opponent.points as opponentPoints,
                        opponent.city AS opponentCity,
                        opponent.image_url as opponentImageUrl,
                        challenge.date as challengeDate,
                        challenge.hour as challengeHour,
                        xp_level.name AS xpLevel,
                        xp_level.id AS xpLevelId
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            game
                        ON
                            myGame.game_id = game.id AND
                            game.status_id = '" . GameStatus::NOT_CONFIRMED . "'
                    RIGHT JOIN
                            challenge
                        ON
                            myGame.game_id = challenge.game_id
                    RIGHT JOIN
                            user_game AS opponentGame
                        ON
                            myGame.game_id = opponentGame.game_id AND
                            opponentGame.user_id != " . $userId . "
                    LEFT JOIN
                            location AS venue
                        ON
                            game.location_id = venue.id
                    LEFT JOIN
                            profile AS opponent
                        ON
                            opponentGame.user_id = opponent.user_id
                    RIGHT JOIN
                            user
                        ON
                            myGame.user_id = user.id
                    RIGHT JOIN
                            xp_level
                        ON
                            user.xp_level_id = xp_level.id
                    WHERE
                        myGame.game_id IN (SELECT game_id FROM user_game WHERE user_id = " . $userId . ")
                        AND myGame.user_id = " . $userId . "
                    ORDER BY
                        game.created_at DESC;";

        $smtp = $this->getEntityManager()->getConnection()->prepare($query);
        $smtp->execute();
        $result = $smtp->fetchAll();

        $upcomingGameResponse = [];

        if (isset($result[0])) {
            $upcomingGame = $result[0];

            $gamesNrResponse = $this->forward('AppBundle\Controller\API\GameController::userGamesCountAction',
                                            [
                                                'request' => $request,
                                                'id'      => $upcomingGame['opponentUserId']
                                            ]
                                        );

            $gamesNr = json_decode($gamesNrResponse->getContent())->games_nr;
            $showChanceToWin = (boolean) ($gamesNr >= 10);

            $pointsResults = $this->getEloSystemRating()->getMatchMakingInfo(
                                                            $points,
                                                            $upcomingGame['opponentPoints'],
                                                            $user->getKFactor());
            $chanceToWin = $pointsResults['chanceToWin'];

            $gameDate = new DateTime($upcomingGame['challengeDate']);
            $gameHour = $upcomingGame['challengeHour'];

            $minutes = round(60 * ($gameHour - floor($gameHour)));
            $hour    = floor($gameHour);

            date_time_set($gameDate, $hour, $minutes);

            $unreadMessagesNr = $this->getUnreadMessagesNr(
                                            $upcomingGame['gameId'],
                                            $upcomingGame['opponentUserId']
                                        );

            $upcomingGameResponse = [
                'id'                        => (int) $upcomingGame['gameId'],
                'chance_to_win'             => (int) $chanceToWin,
                'venue_name'                => $upcomingGame['venueName'],
                'game_date'                 => $gameDate->getTimeStamp(),
                'xp_level'                  => (int) $upcomingGame['xpLevelId'],
                'show_chance_to_win'        => $showChanceToWin,
                'my_score'                  => $upcomingGame['myScore'],
                'opponent_score'            => $upcomingGame['opponentGameScore'],
                'score_inserted_by_user_id' => (int) $upcomingGame['scoreInsertedByUserId'],
                'unread_messages_nr'        => (int) $unreadMessagesNr,
                'profile'       => [
                    'id'         => $upcomingGame['opponentProfileId'],
                    'user_id'    => $upcomingGame['opponentUserId'],
                    'first_name' => $upcomingGame['opponentFirstName'],
                    'last_name'  => $upcomingGame['opponentLastName'],
                    'points'     => $upcomingGame['opponentPoints'],
                    'image_url'  => $upcomingGame['opponentImageUrl']
                ]
            ];
        }

        return new JsonResponse($upcomingGameResponse);
    }

    /**
     *
     * @Get("/games/history")
     *
     * @return JsonResponse
     */
    public function gamesHistoryAction(Request $request)
    {
        $userId = $this->getUser()->getId();

        $query = "SELECT
                        game.id as gameId,
                        myGame.points as myPoints,
                        myGame.score as myScore,
                        venue.name as venueName,
                        game.created_at as gameDate,
                        opponentGame.points as opponentGamePoints,
                        opponentGame.score as opponentGameScore,
                        opponent.user_id as opponentUserId,
                        opponent.id as opponentProfileId,
                        opponent.first_name as opponentFirstName,
                        opponent.last_name as opponentLastName,
                        opponent.points as opponentPoints,
                        opponent.city AS opponentCity,
                        opponent.image_url as opponentImageUrl
                    FROM
                        user_game AS myGame
                    RIGHT JOIN
                            game
                        ON
                            myGame.game_id = game.id AND
                            game.status_id = '" . GameStatus::CLOSED . "'
                    RIGHT JOIN
                            user_game AS opponentGame
                        ON
                            myGame.game_id = opponentGame.game_id AND
                            opponentGame.user_id != " . $userId . "
                    LEFT JOIN
                            location AS venue
                        ON
                            game.location_id = venue.id
                    LEFT JOIN
                            profile AS opponent
                        ON
                            opponentGame.user_id = opponent.user_id
                    WHERE
                        myGame.game_id IN (SELECT game_id FROM user_game WHERE user_id = " . $userId . ")
                        AND myGame.user_id = " . $userId . "
                    ORDER BY
                        game.created_at DESC;";

        $smtp = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $smtp->execute();
        $result = $smtp->fetchAll();

        $gameList = [];

        foreach ($result as $game) {
            $gameList[] = [
                'my_score'        => (int) $game['myScore'],
                'opponent_score'  => (int) $game['opponentGameScore'],
                'points'          => (int) $game['myPoints'],
                'first_name'      => $game['opponentFirstName'],
                'last_name'       => $game['opponentLastName'],
                'image_url'       => $game['opponentImageUrl']
            ];
        }

        return new JsonResponse($gameList);
    }

   /**
     *
     * @Post("/games")
     *
     * @return JsonResponse
     */
    public function newGameAction(Request $request)
    {
        $user               = $this->getUser();
        $opponent           = $this->getUserRepo()->findOneById($request->get('id'));
        $myScore            = 0;
        $opponentScore      = 0;

        $existingGameQuery = "SELECT
                                    COUNT(game.id) as gamesNotClosed
                                FROM
                                    game
                                LEFT JOIN user_game as userGame
                                    ON game.id = userGame.game_id
                                WHERE
                                    userGame.user_id = " . $opponent->getId() . " AND
                                    game.status_id IN (1, 2)";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($existingGameQuery);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $openedGamesCount = $result[0]['gamesNotClosed'];

        if ($openedGamesCount > 0) {
            return new Response('Can\'t create a new game. Opponent already has an open game.', Response::HTTP_CONFLICT);
        }

        if (null !== $request->get('myScore')) {
            $myScore =  $request->get('myScore');
        }

        if (null !== $request->get('opponentScore')) {
            $opponentScore = $request->get('opponentScore');
        }

        $location = $this->get('app.repository.location')->findOneById(1);
        $status = $this->get('app.repository.game_status')->findOneById(1);

        $game = new Game();
        $game->setLocation($location);
        $game->setUser($user);
        $game->setStatus($status);
        $this->getEntityManager()->persist($game);

        $userGame = new UserGame();
        $userGame->setUser($user);
        $userGame->setGame($game);
        $userGame->setScore($myScore);
        $this->getEntityManager()->persist($userGame);

        $opponentGame = new UserGame();
        $opponentGame->setUser($opponent);
        $opponentGame->setGame($game);
        $opponentGame->setScore($opponentScore);
        $this->getEntityManager()->persist($opponentGame);

        $this->getEntityManager()->flush();

        $this->getFirebaseController()->sendGameConfirmationMessage(
            $opponent,
            $game->getId(),
            $this->getFirebaseNotification());

        return new JsonResponse(array('game_id' => (int) $game->getId()));
    }

    // NOT USED ANYMORE FOR NOW
   /**
     *
     * @Put("/games/{id}")
     *
     * @return JsonResponse
     */
    public function updateGameAction(Request $request, $id)
    {
        $userId = $this->getUser()->getId();

        $myScore       = $request->get('myScore');
        $opponentScore = $request->get('opponentScore');

        $game     = $this->getGameRepo()->findOneById($id);
        $userGame = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $now = new DateTime();

        foreach($userGame as $game) {
            if ($userId === $game->getUser()->getId()) {
                $game->setScore($myScore);
            } else {
                $game->setScore($opponentScore);
            }

            $game->setUpdatedAt($now);
            $this->getEntityManager()->persist($game);
        }

        $this->getEntityManager()->flush();

        $this->getFirebaseController()->sendGameConfirmationMessage(
            $opponent,
            $id,
            $this->getFirebaseNotification());

        return new JsonResponse(array('game_status' => 'CLOSED'));
    }

    //TODO: REMOVE THIS ASAP
    /**
     *
     * @Patch("/games/closeall")
     *
     * @return JsonResponse
     */
    public function cloaseAllAction()
    {
        $closeGameQuery = "UPDATE game SET status_id = 3";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($closeGameQuery);
        $dbConn->execute();

        return new JsonResponse(array('all_games_status' => 'CLOSED'));
    }

    /**
     *
     * @Patch("/games/{id}/confirm")
     *
     * @return JsonResponse
     */
    public function confirmGameAction(Request $request, $id)
    {
        $user   = $this->getUser();
        $userId = $user->getId();

        $myScore       = 0;
        $opponentScore = 0;
        $myGame        = null;
        $opponentGame  = null;

        // GET OPPONENT
        $game      = $this->getGameRepo()->findOneById($id);
        $userGames = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $opponent = null;

        foreach($userGames as $userGame) {
            $player = $userGame->getUser();
            if ($userId == $player->getId()) {
                $myGame  = $userGame;
                $myScore = $userGame->getScore();
            } else {
                $opponent      = $player;
                $opponentGame  = $userGame;
                $opponentScore = $userGame->getScore();
            }
        }

        #exception if user's game not found
        if (is_null($myGame)) {
            return new Response('Could not find any related game.', Response::HTTP_FORBIDDEN);
        }

        #update profiles scores and game points
        $profile            = $this->getProfileRepo()->findOneByUser($user);
        $opponentProfile    = $this->getProfileRepo()->findOneByUser($opponent);

        $pointsResults = $this->getEloSystemRating()->getNewRatings(
                                                        $profile->getPoints(),
                                                        $opponentProfile->getPoints(),
                                                        $myScore,
                                                        $opponentScore,
                                                        $user->getKFactor()
                                                    );

        $myPoints       = abs($profile->getPoints() - round($pointsResults['myRating']));
        $opponentPoints = abs($opponentProfile->getPoints() - round($pointsResults['opponentRating']));

        $myGame->setPoints($myPoints);
        $opponentGame->setPoints($opponentPoints);

        $profile->setPoints(round($pointsResults['myRating']));
        $opponentProfile->setPoints(round($pointsResults['opponentRating']));

        $this->getEntityManager()->flush();

        $request->attributes->set('status_id', '3');

        $response = $this->forward('AppBundle\Controller\Api\GameController::patchGameAction',
                                        [
                                            'request' => $request,
                                            'id'       => $id
                                        ]
                                    );

        $this->getFirebaseController()->sendGameClosedMessage(
            $opponent,
            $id,
            $this->getFirebaseNotification());

        # a patch request shoud return the updated part of the object
        $response = [
            'game_id'     => $game->getId(),
            'game_status' => $game->getStatus()->getName()
        ];

        return new JsonResponse($response);
    }

    /**
     *
     * @Patch("/games/{id}/score")
     *
     * @return JsonResponse
     */
    public function saveScoreGameAction(Request $request, $id)
    {
        $user   = $this->getUser();
        $userId = $user->getId();

        $myScore       = 0;
        $opponentScore = 0;
        $myGame        = null;
        $opponentGame  = null;

        // GET OPPONENT
        $game      = $this->getGameRepo()->findOneById($id);
        $userGames = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $opponent = null;

        if (null !== $request->get('my_score')) {
            $myScore =  $request->get('my_score');
        }

        if (null !== $request->get('opponent_score')) {
            $opponentScore = $request->get('opponent_score');
        }

        foreach($userGames as $userGame) {
            $player = $userGame->getUser();
            if ($userId == $player->getId()) {
                $myGame  = $userGame;
            } else {
                $opponent      = $player;
                $opponentGame  = $userGame;
            }
        }

        #exception if user's game not found
        if (is_null($myGame)) {
            return new Response('Could not find any related game.', Response::HTTP_FORBIDDEN);
        }

        #update profiles scores and game points
        $profile            = $this->getProfileRepo()->findOneByUser($user);
        $opponentProfile    = $this->getProfileRepo()->findOneByUser($opponent);

        #TODO - calculate opponent points after it's own KFactor
        $pointsResults = $this->getEloSystemRating()->getNewRatings(
                                                        $profile->getPoints(),
                                                        $opponentProfile->getPoints(),
                                                        $myScore,
                                                        $opponentScore,
                                                        $user->getKFactor()
                                                    );

        $opponentPointsResults = $this->getEloSystemRating()->getNewRatings(
                                                        $opponentProfile->getPoints(),
                                                        $profile->getPoints(),
                                                        $opponentScore,
                                                        $myScore,
                                                        $opponent->getKFactor()
                                                    );
        $myPoints       = abs($profile->getPoints() - round($pointsResults['myRating']));
        $opponentPoints = abs($opponentProfile->getPoints() - round($opponentPointsResults['myRating']));

        $myGame->setPoints($myPoints);
        $myGame->setScore($myScore);

        $opponentGame->setPoints($opponentPoints);
        $opponentGame->setScore($opponentScore);

        $this->getEntityManager()->flush();

        $request->attributes->set('status_id', GameStatus::NOT_CONFIRMED);

        $response = $this->forward('AppBundle\Controller\Api\GameController::patchGameAction',
                                        [
                                            'request' => $request,
                                            'id'       => $id
                                        ]
                                    );

        $challengeStatus = $this->getChallengeStatusRepo()->findOneById(ChallengeStatus::CLOSED);

        $challenge = $this->getChallengeRepo()->findOneByGame($game);
        $challenge->setStatus($challengeStatus);

        $game->setScoreSavedByUser($user);

        $this->getEntityManager()->flush();

        $this->getFirebaseController()->sendValidateScoreMessage(
            $opponent,
            $id,
            $this->getFirebaseNotification());

        # a patch request shoud return the updated part of the object
        $response = [
            'game_id'     => $game->getId(),
            'game_status' => $game->getStatus()->getName()
        ];

        return new JsonResponse($response);
    }

   /**
     *
     * @Patch("/games/{id}/score/accept")
     *
     * @return JsonResponse
     */
    public function acceptScoreGameAction(Request $request, $id)
    {
        $user   = $this->getUser();
        $userId = $user->getId();

        $myScore       = 0;
        $opponentScore = 0;
        $myGame        = null;
        $opponentGame  = null;

        // GET OPPONENT
        $game      = $this->getGameRepo()->findOneById($id);
        $userGames = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $opponent = null;

        foreach($userGames as $userGame) {
            $player = $userGame->getUser();
            if ($userId == $player->getId()) {
                $myGame  = $userGame;
                $myScore = $userGame->getScore();
            } else {
                $opponent      = $player;
                $opponentGame  = $userGame;
                $opponentScore = $userGame->getScore();
            }
        }

        #exception if user's game not found
        if (is_null($myGame)) {
            return new Response('Could not find any related game.', Response::HTTP_FORBIDDEN);
        }

        #update profiles scores and game points
        $profile            = $this->getProfileRepo()->findOneByUser($user);
        $opponentProfile    = $this->getProfileRepo()->findOneByUser($opponent);

        $userPoints     = $profile->getPoints();
        $opponentPoints = $opponentProfile->getPoints();

        $pointsResults = $this->getEloSystemRating()->getNewRatings(
                                                        $userPoints,
                                                        $opponentPoints,
                                                        $myScore,
                                                        $opponentScore,
                                                        $user->getKFactor()
                                                    );

        $opponentPointsResults = $this->getEloSystemRating()->getNewRatings(
                                                        $opponentPoints,
                                                        $userPoints,
                                                        $opponentScore,
                                                        $myScore,
                                                        $opponent->getKFactor()
                                                    );

        $profile->setPoints(round($pointsResults['myRating']));
        $opponentProfile->setPoints(round($opponentPointsResults['myRating']));

        $this->getEntityManager()->flush();

        $request->attributes->set('status_id', GameStatus::CLOSED);

        $response = $this->forward('AppBundle\Controller\Api\GameController::patchGameAction',
                                        [
                                            'request' => $request,
                                            'id'       => $id
                                        ]
                                    );

        $this->getEntityManager()->flush();

        $userKFactor     = $user->getKFactor();
        $opponentKFactor = $opponent->getKFactor();

        $userGamesNrResponse = $this->forward('AppBundle\Controller\API\GameController::userGamesCountAction',
                                        [
                                            'request' => $request,
                                            'id'      => $userId
                                        ]
                                    );
        $userGamesNr = json_decode($userGamesNrResponse->getContent())->games_nr;

        $opponentGamesNrResponse = $this->forward('AppBundle\Controller\API\GameController::userGamesCountAction',
                                        [
                                            'request' => $request,
                                            'id'      => $opponent->getId()
                                        ]
                                    );
        $opponentGamesNr = json_decode($opponentGamesNrResponse->getContent())->games_nr;

        $userNewKFactor     = $this->getEloSystemRating()->getNewKFactor($userGamesNr, $userKFactor, $userPoints);
        $opponentNewKFactor = $this->getEloSystemRating()->getNewKFactor($opponentGamesNr, $opponentKFactor, $opponentPoints);

        $user->setKFactor($userNewKFactor);
        $opponent->setKFactor($opponentNewKFactor);

        $this->getEntityManager()->flush();

        $this->getFirebaseController()->sendScoreConfirmedMessage(
            $opponent,
            $id,
            $this->getFirebaseNotification());

        // $this->changePaymentStatus($userId);
        // $this->changePaymentStatus($opponent->getId());

        # a patch request shoud return the updated part of the object
        $response = [
            'game_id'     => $game->getId(),
            'game_status' => $game->getStatus()->getName()
        ];

        return new JsonResponse($response);
    }

  /**
     *
     * @Patch("/games/{id}/score/reject")
     *
     * @return JsonResponse
     */
    public function rejectScoreGameAction(Request $request, $id)
    {
        $user   = $this->getUser();
        $userId = $user->getId();

        $myScore       = 0;
        $opponentScore = 0;
        $myGame        = null;
        $opponentGame  = null;

        $reason['reason']      = $request->get('reason');
        $reason['wrong_score'] = $request->get('wrong_score_reason');

        // GET OPPONENT
        $game      = $this->getGameRepo()->findOneById($id);
        $userGames = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $opponent = null;

        foreach($userGames as $userGame) {
            $player = $userGame->getUser();
            if ($userId != $player->getId()) {
                $opponent = $player;
            }
        }

        #exception if user's game not found
        if (is_null($game)) {
            return new Response('Could not find any related game.', Response::HTTP_FORBIDDEN);
        }

        $request->attributes->set('status_id', GameStatus::REJECTED);

        $response = $this->forward('AppBundle\Controller\Api\GameController::patchGameAction',
                                        [
                                            'request' => $request,
                                            'id'       => $id
                                        ]
                                    );

        $this->getFirebaseController()->sendGameRejectedMessage(
            $opponent,
            $id,
            $reason,
            $this->getFirebaseNotification());

        // $this->changePaymentStatus($userId);
        // $this->changePaymentStatus($opponent->getId());

        # a patch request shoud return the updated part of the object
        $response = [
            'game_id'     => $game->getId(),
            'game_status' => $game->getStatus()->getName()
        ];

        return new JsonResponse($response);
    }

    /**
     *
     * @Patch("/games/{id}/cancel")
     *
     * @return JsonResponse
     */
    public function cancelGameAction(Request $request, $id)
    {
        $user   = $this->getUser();
        $userId = $user->getId();

        $myScore       = 0;
        $opponentScore = 0;
        $myGame        = null;
        $opponentGame  = null;

        $reason['reason']          = $request->get('reason');
        $reason['personal_reason'] = $request->get('personal_reason');

        // GET OPPONENT
        $game      = $this->getGameRepo()->findOneById($id);
        $userGames = $this
                    ->getUserGameRepo()
                    ->findByGame($game);

        $opponent = null;

        foreach($userGames as $userGame) {
            $player = $userGame->getUser();
            if ($userId != $player->getId()) {
                $opponent = $player;
            }
        }

        #exception if user's game not found
        if (is_null($game)) {
            return new Response('Could not find any related game.', Response::HTTP_FORBIDDEN);
        }

        $challenge = $this->getChallengeRepo()->findOneByGame($game);

        $challengeDate = $challenge->getDate()->format('Y-m-d');
        $challengeHour = $challenge->getHour();

        $hoursRemaining = self::getHoursRemaining($challengeDate, $challengeHour);

        $refund = true;
        if ($hoursRemaining < self::FREE_GAME_CANCELATION_HOURS) {
            $refund = false;
            // return new Response('You can only cancel a game 24 hours before the actual game.', Response::HTTP_FORBIDDEN);
        }

        $request->attributes->set('status_id', GameStatus::CANCELED);

        $response = $this->forward('AppBundle\Controller\Api\GameController::patchGameAction',
                                        [
                                            'request' => $request,
                                            'id'      => $id
                                        ]
                                    );

        $challengeStatus = $this->getChallengeStatusRepo()->findOneById(ChallengeStatus::CLOSED);

        $challenge->setStatus($challengeStatus);

        $this->getEntityManager()->flush();

        VenueController::bookCourt(
            $challenge->getDate()->format('Y-m-d'),
            $challenge->getHour(),
            VenueController::VENUE_FREE_STATUS
        );

        $this->getFirebaseController()->sendGameCanceledMessage(
            $opponent,
            $id,
            $reason,
            $this->getFirebaseNotification());

        $profile = $this->getProfileRepo()->findOneByUser($opponent);
        $this->getSwiftEmailManagerService()->sendCancellationEmail(
            $challenge->getHour(),
            $challenge->getDate()->format('Y-m-d'),
            $profile->getFirstName() . ' ' .$profile->getLastName()
        );

        //UNCOMMENT THEESE NEXT LINES FOR ACTIVATIONG REFUND
        // if ($refund){
        //     $paymentHash = PaymentController::getUserLastPaymentHash($userId);
        //     if ($paymentHash) {
        //         $this->getPayment()->refundTransaction($paymentHash);
        //     }

        //     $opponentPaymentHash = PaymentController::getUserLastPaymentHash($opponnent->getId());
        //     if ($opponentPaymentHash) {
        //         $this->getPayment()->refundTransaction($opponentPaymentHash);
        //     }
        //     $newPaymentStatus = PaymentStatus::REFUNDED;
        // } else {
        //     $newPaymentStatus = PaymentStatus::CANCELED;
        // }

        // $this->changePaymentStatus(
        //     $userId,
        //     PaymentStatus::USED,
        //     $newPaymentStatus);
        // $this->changePaymentStatus(
        //     $opponent->getId(),
        //     PaymentStatus::USED,
        //     $newPaymentStatus);

        # a patch request shoud return the updated part of the object
        $response = [
            'game_id'     => $game->getId(),
            'game_status' => $game->getStatus()->getName(),
            'refund'      => $refund
        ];

        return new JsonResponse($response);
    }

    /**
     *
     * @Patch("/games/{id}")
     *
     * @return JsonResponse
     */
    public function patchGameAction(Request $request, $id)
    {
        $game = $this->getGameRepo()->findOneById($id);

        $gameStatusId = $request->get('status_id');

        if (isset($gameStatusId)) {
            $status = $this->getGameStatusRepo()->findOneById($gameStatusId);
            $game->setStatus($status);
        }

        $this->getEntityManager()->persist($game);
        $this->getEntityManager()->flush();

        return new JsonResponse(array('game_id' => $game->getId()));
    }

    public function getUnreadMessagesNr($gameId, $opponentId)
    {
        $query = "SELECT
                        COUNT(id) AS messagesNr
                    FROM
                        conversation
                    WHERE
                        user_id = " . $opponentId . " AND
                        game_id = " . $gameId . " AND
                        read_status = " . Conversation::NOT_READ_STATUS . ";";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        return $result[0]['messagesNr'];
    }

    public function getHoursRemaining($challengeDate, $challengeHour)
    {
        $challengeHourValue    = date('G', strtotime($challengeHour));
        $challengeMinutesValue = gmdate('i', round(($challengeHour - floor($challengeHour)) * 3600));

        $stringTime =
            $challengeDate . ' ' .
            $challengeHourValue . ':' .
            $challengeMinutesValue . ':00';

        $gameDate = strtotime($stringTime);

        $dateTime = date('Y-m-d H:i:s', $gameDate);

        $localTimeZone = new DateTimeZone('Europe/Bucharest');

        $gameDate = new DateTime($stringTime, $localTimeZone);
        $dateNow = new DateTime();

        if ($gameDate < $dateNow) {
            return 0;
        }

        $timeRemaining = $dateNow->diff($gameDate);

        $hoursRemaining = (int) $timeRemaining->format('%h');
        $daysRemaining = (int) $timeRemaining->format('%d');

        return $daysRemaining * 24 + $hoursRemaining;
    }


    public function changePaymentStatus(
        $userId,
        $oldStatus = PaymentStatus::OPENED,
        $newStatus = PaymentStatus::USED
        )
    {
        $query = "  UPDATE
                        payment
                    SET
                        status_id = " . $newStatus . "
                    WHERE
                            transaction_status_id = " . PaymentTransactionStatus::CONFIRMED  . "
                        AND
                            status_id = " . $oldStatus . "
                        AND
                            user_id = " . $userId . "
                        ;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
    }

}

<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\GameStatus;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class LeaderBoardController extends BaseController
{
    /**
     *
     * @Get("/users")
     *
     * @return JsonResponse
     */
    public function leaderBoardAction(Request $request)
    {
        $query = "SELECT
                        p.points AS points,
                        p.first_name AS firstName,
                        p.last_name AS lastName,
                        p.image_url AS imageUrl,
                        p.id AS profileId,
                        u.id AS userId,
                        COUNT(ug.id) AS games
                    FROM
                        user AS u
                    INNER JOIN
                            user_game AS ug
                        ON
                            ug.user_id = u.id
                    JOIN
                            game as g
                        ON
                            g.id = ug.game_id
                    RIGHT JOIN
                            profile AS p
                        ON
                            p.user_id = u.id
                    WHERE
                        g.status_id = " . GameStatus::CLOSED . "
                    GROUP BY
                        userId, lastName, firstName, points, imageUrl, profileId
                    ORDER BY
                        points DESC, games DESC, firstName DESC, lastName DESC;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $profiles = $dbConn->fetchAll();

        $leaderboard = [];
        $ranking  = 0;

        foreach ($profiles as $profile) {
            $ranking++;
            $leaderboard[] = [
                'id'      => (int) $profile['userId'],
                'profile' => [
                    'id'         => (int) $profile['profileId'],
                    'first_name' => $profile['firstName'],
                    'last_name'  => $profile['lastName'],
                    'image_url'  => $profile['imageUrl'],
                    'points'     => (int) $profile['points'],
                    'games'      => (int) $profile['games'],
                    'ranking'    => $ranking
                ]
            ];
        }

        return new JsonResponse($leaderboard);
    }
}

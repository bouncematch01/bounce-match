<?php

namespace AppBundle\Controller\API;

use AppBundle\Validator\Constraints as QubizUserAssert;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\OAuthServerBundle\Model\ClientInterface;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use OAuth2\OAuth2;
use OAuth2\OAuth2ServerException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class OAuthController extends BaseController
{
    /**
     * Register a new OAuth Client.
     *
     * @Post("/oauth/v2/register")
     *
     * @return JsonResponse
     */
    public function registerAction(Request $request)
    {
        $clientManager = $this->getOAuthClientManager();

        /** @var Client $client */
        $client = $clientManager->createClient();
        $client->setAllowedGrantTypes([OAuth2::GRANT_TYPE_USER_CREDENTIALS, OAuth2::GRANT_TYPE_REFRESH_TOKEN]);
        $clientManager->updateClient($client);

        return new JsonResponse([
            'client_id'     => $client->getPublicId(),
            'client_secret' => $client->getSecret()
        ]);
    }

    /**
     * Create a token from the submitted data.
     *
     * @Post("/oauth/v2/token/{network}", requirements={"network": "facebook"})
     *
     * @return JsonResponse
     */
    public function tokenAction(Request $request, $network)
    {
        try {
            $oauthResponse = $this->getOAuthServer()->grantAccessToken($request, $network);
        } catch (OAuth2ServerException $e) {
            return $e->getHttpResponse();
        } catch (Exception $e) {
            // TODO: handle db exception
        }

        if ($request->request->get('grant_type') === OAuth2::GRANT_TYPE_REFRESH_TOKEN) {
            return new JsonResponse([
                'oauth' => json_decode($oauthResponse->getContent())
            ]);
        }

        $user = $this->get('app.repository.user')
                     ->findOneByUsername($request->request->get('username'));

        $profile = $this->get('app.repository.profile')->findOneByUser($user);

        $query = "select count(*) as profileNr from profile";
        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $profilesNr = $result[0]['profileNr'];

        $query = "SELECT
                        COUNT(id) AS position
                    FROM
                        profile
                    WHERE
                        points <= " . $profile->getPoints() . " AND id = " . $profile->getId();
        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $position = $result[0]['position'];

        $query = "SELECT
                        COUNT(id) AS userGames
                    FROM
                        user_game
                    WHERE
                        user_id = " . $user->getId();
        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $games = $result[0]['userGames'];

        $query = "SELECT
                        COUNT(*) AS profileNr
                    FROM
                        profile";
        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $profilesNr = $result[0]['profileNr'];


        #query for same rank if same points
        // $query = "SET @prev_value = NULL;
        //             SET @rank_count = 0;
        //             SELECT id, points, CASE
        //                 WHEN @prev_value = points THEN @rank_count
        //                 WHEN @prev_value := points THEN @rank_count := @rank_count + 1
        //             END AS rank
        //             FROM profile
        //             ORDER BY points DESC;";

        #query for rank without seeparate var intialization
        $query = "SELECT * FROM (
                        SELECT
                            user_id, points, @position := @position + 1 AS rank
                        FROM
                            profile p, (SELECT @position := 0) r
                        ORDER BY
                            points DESC, created_at ASC
                    )
                    AS allRanking
                    WHERE allRanking.user_id = " . $user->getId();

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $position = $result[0]['rank'];

        $query = "SELECT
                        COUNT(id) AS userGames
                    FROM
                        user_game
                    WHERE
                        user_id = " . $user->getId();
        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $games = $result[0]['userGames'];

        return new JsonResponse([
            'oauth' => json_decode($oauthResponse->getContent()),
            'user'  => [
                'id'        => $user->getId(),
                'email'     => $user->getEmail(),
                'onboarded' => $user->getOnboarded(),
                'profile'   => [
                    'id'            => $profile->getId(),
                    'first_name'    => $profile->getFirstName(),
                    'last_name'     => $profile->getLastName(),
                    'image_url'     => $profile->getImageUrl(),
                    'city'          => $profile->getCity(),
                    'country'       => $profile->getCountry(),
                    'venue'         => $profile->getLocation()->getName(),
                    'points'        => $profile->getPoints(),
                    'ranking'       => (int) $position,
                    'total_players' => (int) $profilesNr,
                    'games'         => (int) $games
                ]
            ]
        ]);
    }


}

<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\ChallengeStatus;
use AppBundle\Entity\GameStatus;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class OpponentController extends BaseController
{
    const FRONT_CHANCE_TO_WIN_START     = 50;
    const FRONT_LOWER_INTERVAL          = 40;
    const FRONT_GREATER_INTERVAL        = 60;
    const CATEGORY_OPPONENT_TYPE_TOTAL  = 'total';
    const CATEGORY_OPPONENT_TYPE_BETTER = 'better';
    const CATEGORY_OPPONENT_TYPE_SAME   = 'same';
    const CATEGORY_OPPONENT_TYPE_LOWER  = 'lower';
    const STILL_BEGGINER_GAMES_COUNT    = 5;

    /**
     *
     * @Get("/opponents")
     *
     * @return JsonResponse
     */
    public function opponentAction(Request $request)
    {
        $userId   = $this->getUser()->getId();
        $query = "
                    SELECT
                        id, user_id, first_name, last_name, points, image_url
                    FROM
                        profile
                    WHERE
                        user_id != " . $userId . "
                    ORDER BY
                        points DESC, created_at ASC;";

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $profiles = $dbConn->fetchAll();

        $opponentlist = [];

        foreach ($profiles as $profile) {
            $opponentlist[] = array (
                'id'      => (int) $profile['user_id'],
                'profile' => [
                    'id'         => (int) $profile['id'],
                    'first_name' => $profile['first_name'],
                    'last_name'  => $profile['last_name'],
                    'points'     => (int) $profile['points'],
                    'image_url'  => $profile['image_url']
                ]
            );
        }

        return new JsonResponse($opponentlist);
    }

    /**
     *
     * @Post("/matchmaking")
     *
     * @return JsonResponse
     */
    public function matchmakingAction(Request $request)
    {
        $user    = $this->getUser();
        $profile = $this->getProfileRepo()->findOneByUser($user);

        $myPoints = $profile->getPoints();

        $intervals = $request->get('intervals');
        $venueId   = $request->get('venue_id');
        $date      = $request->get('date');

        if (!isset($date)) {
            return new Response('Date not sent.', Response::HTTP_BAD_REQUEST);
        }

        $weekDayId = date('N', $date);

        $weekDay   = $this->getWeekDayRepo()->findOneById($weekDayId);

        if (!isset($weekDay)) {
            return new Response('Wrong day.', Response::HTTP_NOT_FOUND);
        }

        if (!isset($intervals)) {
            return new Response('Intervals not received.', Response::HTTP_BAD_REQUEST);
        }

        $filter = [
            'week_day_id'        => $weekDay->getId(),
            'user_id'            => $user->getId(),
            'venue_id'           => $venueId,
            'intervals'          => $intervals,
            'challenge_date'     => $date
        ];

        $query = self::getUsersWithChallengesQuery($filter);

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $usersWithChallengesSent = $dbConn->fetchAll();

        $usersWithChallengesSendQuery = '';

        if (!empty($usersWithChallengesSent)) {
            $usersWithChallengesSendQuery = " AND matchMaking.userId NOT IN (";

            foreach($usersWithChallengesSent as $userWithChallengesSent) {
                $usersWithChallengesSendQuery .= $userWithChallengesSent['userId'];
                if ($userWithChallengesSent !== end($usersWithChallengesSent)) {
                    $usersWithChallengesSendQuery .= ", ";
                } else {
                    $usersWithChallengesSendQuery .= ")";
                }
            }
        }

        $filter['users_with_challenges_send_query'] = $usersWithChallengesSendQuery;

        $query = self::getUsersWithUpcomingGamesQuery();

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $usersWithUpcomingGame = $dbConn->fetchAll();

        $usersWithUpcomingGameQuery = '';

        if (!empty($usersWithUpcomingGame)) {

            $usersWithUpcomingGameQuery = " AND matchMaking.userId NOT IN (";

            foreach($usersWithUpcomingGame as $userWithUpcomingGame) {
                $usersWithUpcomingGameQuery .= $userWithUpcomingGame['userId'];
                if ($userWithUpcomingGame !== end($usersWithUpcomingGame)) {
                    $usersWithUpcomingGameQuery .= ", ";
                } else {
                    $usersWithUpcomingGameQuery .= ")";
                }
            }
        }

        $filter['users_with_upcoming_game_query'] = $usersWithUpcomingGameQuery;

        $query = self::getMatchMakingQuery($filter);

        $dbConn = $this->get('doctrine.orm.entity_manager')->getConnection()->prepare($query);
        $dbConn->execute();
        $opponents = $dbConn->fetchAll();

        $opponentlist    = [];
        $chanceToWinTempList = [];

        foreach ($opponents as $opponent) {
            $gamesNrResponse = $this->forward('AppBundle\Controller\API\GameController::userGamesCountAction',
                                            [
                                                'request' => $request,
                                                'id'      => $opponent['userId']
                                            ]
                                        );

            $challenge = json_decode(
                $this->forward('AppBundle\Controller\API\ChallengeController::userChallangeAction',
                                        [
                                            'request' => $request,
                                            'id'      => $opponent['userId']
                                        ]
            )->getContent());

            $opponentInfo = [];
            $challengeInfo = [];
            if (!empty($challenge)){
                $challengeInfo['challenge'] = [
                    'id'        => $challenge[0]->challenge_id,
                    'location'  => $challenge[0]->venue_name,
                    'date'      => $challenge[0]->date,
                    'intervals' => $challenge[0]->intervals
                ];
            }

            $gamesNr = json_decode($gamesNrResponse->getContent())->games_nr;
            $showChanceToWin = (boolean) ($gamesNr >= self::STILL_BEGGINER_GAMES_COUNT);

            $pointsResults = $this->getEloSystemRating()->getMatchMakingInfo(
                                    $myPoints,
                                    (int) $opponent['points'],
                                    $user->getKFactor()
                                );

            $chanceToWin = $pointsResults['chanceToWin'];

            $cardsForChanceToWin = 0;
            if (isset($chanceToWinTempList[$chanceToWin]['cardsNr'])) {
                $cardsForChanceToWin = $chanceToWinTempList[$chanceToWin]['cardsNr'];
            }

            if (!isset($chanceToWinTempList[$chanceToWin]['scrollDisplayChanceToWin']) && $showChanceToWin) {
                $chanceToWinTempList[$chanceToWin]['scrollDisplayChanceToWin'] = true;
            }

            if (!isset($chanceToWinTempList[$chanceToWin]['xpLevelName'])) {
                $chanceToWinTempList[$chanceToWin]['xpLevelName'] = $opponent['xpLevel'];
            }

            $chanceToWinTempList[$chanceToWin]['cardsNr'] = $cardsForChanceToWin + 1;

            $opponentInfo = [
                'id'                 => (int) $opponent['userId'],
                'xp_level'           => (int) $opponent['xpLevelId'],
                'show_chance_to_win' => $showChanceToWin,
                'chance_to_win'      => $chanceToWin,
                'points_to_win'      => $pointsResults['pointsToWin'],
                'first_name'         => $opponent['firstName'],
                'last_name'          => $opponent['lastName'],
                'points'             => (int) $opponent['points'],
                'image_url'          => $opponent['profileImage']
            ];

            if (!empty($challengeInfo)) {
                $opponentInfo['challenge'] = $challengeInfo['challenge'];
            }

            $opponentlist[] = $opponentInfo;
        }

        $chanceToWinList = [];
        $firstVisible = null;

        foreach($chanceToWinTempList as $chanceToWin => $cards) {
            #get the closest value of chance_to_win to 50
            if (
                $firstVisible === null ||
                abs(self::FRONT_CHANCE_TO_WIN_START - $firstVisible) >
                abs($chanceToWin - self::FRONT_CHANCE_TO_WIN_START)
            ){
                 $firstVisible = $chanceToWin;
            }

            $xpLevel = $chanceToWin;
            $showChanceToWinScroll = false;
            $xpLevel = $chanceToWinTempList[$chanceToWin]['xpLevelName'];

            if (isset($cards['scrollDisplayChanceToWin']) && $cards['scrollDisplayChanceToWin']) {
                $showChanceToWinScroll = true;
            }

            #create chance to win array for FE
            $chanceToWinList[] = [
                'chance_to_win' => $chanceToWin,
                'show_chance_to_win' => $showChanceToWinScroll,
                'cards_nr'      => $cards['cardsNr'],
                'xp_level' => $xpLevel
            ];
        }

        $response = [
            'first_visible'       => $firstVisible,
            'chance_to_win_list'  => $chanceToWinList,
            'players'             => $opponentlist
        ];

        return new JsonResponse($response);
    }

    private function getUsersWithUpcomingGamesQuery()
    {
        $query = " SELECT
                        ug.user_id  as userId
                    FROM
                        user_game as ug
                    RIGHT JOIN
                            game
                        ON
                            ug.game_id = game.id
                        WHERE
                            game.status_id = " . GameStatus::UPCOMING_GAME . "
                    GROUP BY
                        userId";

        return $query;
    }

    private function getMatchMakingQuery($filter)
    {
        $weekDayId    = $filter['week_day_id'];
        $userId       = $filter['user_id'];
        $venueId      = $filter['venue_id'];
        $intervals    = $filter['intervals'];

        $excludeUsersWithChallengesQuery    = $filter['users_with_challenges_send_query'];
        $excludeUsersWithUpcomingGamesQuery = $filter['users_with_upcoming_game_query'];

        $venueFilter = "";

        if (isset($venueId)) {
            $venueFilter = " AND profile.location_id = " . $venueId;
        }

        $intervalQuery = "";
        $filterNr = count($intervals);

        foreach($intervals as $interval){
            $start = $interval['start'];
            $end   = $interval['end'];

            if ($start === $end) {
                $end++;
            }

            $intervalQuery .= " ((" . $start . " <  ua.start AND " . $end . " > ua.start) OR
                            (" . $start . " <=  ua.end AND " . $end . " > ua.end) OR
                            (" . $start . " >  ua.start AND " . $end . " <= ua.end)) ";

            if ($interval !== end($intervals)) {
                $intervalQuery .= " OR ";
            } else {
                $intervalQuery .= " AND ";
            }
        }

        $query = " SELECT * FROM(
                        SELECT
                            ua.user_id AS userId,
                            user.email AS email,
                            xp_level.name AS xpLevel,
                            xp_level.id AS xpLevelId,
                            profile.points AS points,
                            profile.first_name AS firstName,
                            profile.last_name AS lastName,
                            profile.image_url AS profileImage,
                            ua.week_day_id AS dayId
                        FROM
                            availability AS ua
                        RIGHT JOIN
                                user
                            ON
                                ua.user_id = user.id AND
                                user.id != " . $userId . "
                        RIGHT JOIN
                                xp_level
                            ON
                                user.xp_level_id = xp_level.id
                        RIGHT JOIN
                                profile
                            ON
                                user.id = profile.user_id " . $venueFilter . "
                        WHERE
                            " . $intervalQuery ."
                            ua.week_day_id = " . $weekDayId . " AND
                            ua.user_id != " . $userId  . "
                    ) AS matchMaking
                    WHERE
                        1 = 1
                    " . $excludeUsersWithChallengesQuery .
                    $excludeUsersWithUpcomingGamesQuery . "
                    AND matchMaking.dayId = " . $weekDayId . "
                    GROUP BY
                        matchMaking.userId,
                        matchMaking.points,
                        matchMaking.firstName,
                        matchMaking.lastName,
                        matchMaking.profileImage,
                        matchMaking.dayId
                    ORDER BY
                        matchMaking.points ASC";

        return $query;
    }

    private function getUsersWithChallengesQuery($filter)
    {
        $challengeDate = date('Y-m-d', $filter['challenge_date']);
        $userId        = $filter['user_id'];
        $venueId       = $filter['venue_id'];
        $intervals     = $filter['intervals'];

        $venueFilter = "";

        if (isset($venueId)) {
            $venueFilter = " AND profile.location_id = " . $venueId;
        }

        $intervalQuery = "";
        $filterNr = count($intervals);

        foreach($intervals as $interval){
            $start = $interval['start'];
            $end   = $interval['end'];

            if ($start === $end) {
                $end++;
            }

            $intervalQuery .= " ((" . $start . " <  ci.start AND " . $end . " > ci.start) OR
                            (" . $start . " <=  ci.end AND " . $end . " > ci.end) OR
                            (" . $start . " >  ci.start AND " . $end . " <= ci.end)) ";

            if ($interval !== end($intervals)) {
                $intervalQuery .= " OR ";
            } else {
                $intervalQuery .= " AND ";
            }
        }

        $query = " SELECT
                        user.id AS userId
                    FROM
                        challenge_interval AS ci
                    RIGHT JOIN
                            challenge
                        ON
                            ci.challenge_id = challenge.id
                    RIGHT JOIN
                            user_game
                        ON
                            challenge.game_id = user_game.game_id
                    RIGHT JOIN
                            game
                        ON
                            challenge.game_id = game.id
                    RIGHT JOIN
                            user
                        ON
                            user_game.user_id = user.id
                            AND
                            user_game.user_id != " . $userId . "
                    RIGHT JOIN
                            profile
                        ON
                            user.id = profile.user_id " . $venueFilter . "
                    WHERE
                        DATE(challenge.date) = '" . $challengeDate . "' AND
                        challenge.status_id = " . ChallengeStatus::NOT_CONFIRMED . " AND
                        user.id != " . $userId . " AND
                        game.created_by_user_id = " . $userId . "
                    GROUP BY userId ";

        return $query;
    }
}

<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\PaymentStatus;
use AppBundle\Entity\PaymentTransactionStatus;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class PaymentController extends BaseController
{
    /**
     * @Get("/payment-status")
     *
     * @return JsonResponse
     */
    public function paymentAction(Request $request)
    {
        $userId = $this->getUser()->getId();

        $query = "SELECT
                        id
                    FROM
                        payment
                    WHERE
                            transaction_status_id = " . PaymentTransactionStatus::CONFIRMED  . "
                        AND
                            status_id = " . PaymentStatus::OPENED . "
                        AND
                            user_id = " . $userId . "
                        ;";

        $smtp = $this->getEntityManager()->getConnection()->prepare($query);
        $smtp->execute();
        $result = $smtp->fetchAll();

        $paymentDone = true;
        if (empty($result)) {
            $paymentDone = false;
        }

        $response = [
            'payment_done' => $paymentDone
        ];

        return new JsonResponse($response);
    }

    public function getUserLastPaymentHash($userId)
    {
        $query = "SELECT
                        id, payment_id
                    FROM
                        payment
                    WHERE
                        user_id = " . $userId . "
                    ORDER BY
                        id DESC
                    LIMIT 0, 1
                        ;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();

        if (empty($result)) {
            return false;
        }

        return $result[0]['payment_id'];
    }
}

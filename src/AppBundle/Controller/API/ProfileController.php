<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\Location;
use AppBundle\Entity\City;
use DateTime;
use Exception;
use Facebook\Facebook;
use Facebook\GraphNodes\GraphUser;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProfileController extends BaseController
{
    /**
     *
     * @Patch("/profiles/venue")
     * @ApiDoc(
     *     description="Update profile location.",
     *     headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + user_token",
     *             "required"=true
     *         }
     *     },
     *      parameters={
     *          {"name"="id", "dataType"="integer", "required"=true, "description"="Venue Id"}
     *      },
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "requirement"="\d+",
     *             "description"="Venue id"
     *          }
     *      },
     *     resource=true,
     *     section="Profile",
     *     statusCodes = {
     *         Response::HTTP_OK = "Returned when successful. With 'UPDATED'",
     *         Response::HTTP_NOT_FOUND = "Returned when venue not found.",
     *     }
     *)
     * @return JsonResponse
     */
    public function updateProfileLocationAction(Request $request)
    {
        $venueId = $request->get('id');
        $response = $this->forward('AppBundle\Controller\Api\ProfileController::patchProfileAction',
                                        ['venue_id' => $venueId]
                                    );

        return $response;
    }

    /**
     * @Patch("/profiles/from-facebook")
     * @ApiDoc(
     *     description="Update profile from facebook.",
     *     output={
     *     "class" = "AppBundle\Entity\Profile",
     *     "groups"={"image_url"}
     *     },
     *     headers={
     *         {
     *             "name"="Authorization",
     *             "description"="Bearer + user_token",
     *             "required"=true
     *         }
     *     },
     *     resource=true,
     *     section="Profile",
     *     statusCodes = {
     *         Response::HTTP_OK = "Returned when successful.",
     *         Response::HTTP_NOT_FOUND = "Returned when venue not found.",
     *     }
     *)
     * @return JsonResponse
     */
    public function updateProfileFromFacebookAction(Request $request)
    {
        #TODO - CHANGE THIS METHOD - it's not secure and this kind of functionality should not be here
        $facebook = new Facebook([
          'app_id'                => $this->container->getParameter('facebook.app_id'),
          'app_secret'            => $this->container->getParameter('facebook.secret'),
          'default_graph_version' => $this->container->getParameter('facebook.graph_version')
        ]);

        try {
            $socialToken = $this->getUser()->getFacebookAccessToken();

            $facebookProfileMe = '/me?fields=id,email,first_name,last_name,cover,picture.height(480).width(480),location{location}';

            $result = $facebook->get($facebookProfileMe, $socialToken);

            $socialDetails = $result->getGraphUser();

            if (!$socialDetails->getId()) {
                return new Response('Not updated.', Response::HTTP_NOT_FOUND);
            }
        } catch (Exception $e) {
            // TODO: update message thrown here -> crt looks like bad pass / user
            $this->container->get('logger')->error($e);

            return new Response('Not updated.', Response::HTTP_NOT_FOUND);
        }

        if (!is_null($socialDetails->getPicture())) {
            $profileImage = $socialDetails->getPicture()->getUrl();
            $response = $this->forward('AppBundle\Controller\Api\ProfileController::patchProfileAction',
                                            ['profile_image' => $profileImage]
                                        );
        }

        $user = $this->getUser();
        $profile = $this->getProfileRepo()->findOneByUser($user);

        return new JsonResponse([
            'image_url' => $profileImage
        ]);
    }

    /**
     *
     * @Patch("/profiles")
     *
     * @return JsonResponse
     */
    public function patchProfileAction(Request $request)
    {
        $user = $this->getUser();
        $profile = $this->getProfileRepo()->findOneByUser($user);

        $venueId      = $request->get('venue_id');
        $profileImage = $request->get('profile_image');

        if (isset($venueId)) {
            $venue = $this->getLocationRepo()->findOneById($venueId);
            if (is_null($venue)) {
                return new Response('Wrong venue.', Response::HTTP_NOT_FOUND);
            }

            $profile->setLocation($venue);
        }

        if (isset($profileImage)) {
            $profile->setImageUrl($profileImage);
        }


        $this->getEntityManager()->persist($profile);
        $this->getEntityManager()->flush();

        return new JsonResponse('UPDATED');
    }

    /**
     *
     * @Get("/profiles/dashboard")
     *
     * @return JsonResponse
     */
    public function dashboardProfileAction(Request $request)
    {
        $user = $this->getUser();
        $city = $this->getCityRepo()->findOneById(City::DEFAULT_CITY_ID);

        $dashboardProfile = json_decode(
            $this->forward('AppBundle\Controller\API\UserController::userAction',
                                    [
                                        'request' => $request,
                                        'id'      => $user->getId()
                                    ]
        )->getContent());

        unset($dashboardProfile->profile->games_against_me);
        unset($dashboardProfile->email);
        unset($dashboardProfile->id);

        $dashboardProfile->profile->city = $city->getName();

        #TODO - make this a non hardcoded calue
        $dashboardProfile->profile->city_image_url = 'http://52.212.206.23/images/city-cluj.png';

        $gamesWon = json_decode(
            $this->forward('AppBundle\Controller\API\GameController::gamesWonAction',
                                    [
                                        'request' => $request
                                    ]
        )->getContent());

        $totalGames = $dashboardProfile->profile->games;
        $winRate    = 0;

        if (isset($dashboardProfile->profile->games) && $totalGames > 0) {
            $gamesWonNr = 0;

            if (isset($gamesWon->games_won)) {
                $gamesWonNr = $gamesWon->games_won;
            }

            $winRate = round(($gamesWonNr * 100) / $totalGames);
        }

        $gamesLostNr = $totalGames - $gamesWonNr;

        $incomingChallengesNr = json_decode(
            $this->forward('AppBundle\Controller\API\ChallengeController::getIncomingChallengesNrAction')->getContent()
        );

        $upcomingGame = json_decode(
            $this->forward('AppBundle\Controller\API\GameController::getUpcomingGameAction')->getContent()
        );

        $notConfirmedGame = json_decode(
            $this->forward('AppBundle\Controller\API\GameController::getNotConfirmedGameAction')->getContent()
        );

        $dashboardProfile->dashboard = [
            'win_rate'                => (int) $winRate,
            'games_won'               => (int) $gamesWonNr,
            'games_lost'              => (int) $gamesLostNr,
            'has_incoming_challenges' => (bool) ($incomingChallengesNr->challenges_nr > 0),
            'has_upcoming_game'       => (bool) (isset($upcomingGame->id)),
            'has_not_confirmed_game'  => (bool) (isset($notConfirmedGame->id)),
            'has_closed_game'         => (bool) ($dashboardProfile->profile->games > 0)
        ];

        return new JsonResponse($dashboardProfile);
    }
}

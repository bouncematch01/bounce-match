<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\GameStatus;
use DateTime;
use Exception;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Put;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends BaseController
{
    /**
     *
     * @Get("/users/{id}")
     *
     * @return JsonResponse
     */
    public function userAction(Request $request, $id)
    {
        $user    = $this->getUserRepo()->findOneById($id);
        $profile = $this->getProfileRepo()->findOneByUser($user);

        if (NULL === $profile) {
            return new Response('profile_not_found', Response::HTTP_NOT_FOUND);
        }

        $query = "SELECT
                        COUNT(*) AS profileNr
                    FROM
                        profile";
        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $profilesNr = $result[0]['profileNr'];

        $query = "SELECT * FROM (
                        SELECT
                            user_id, points, @position := @position + 1 AS rank
                        FROM
                            profile p, (SELECT @position := 0) r
                        ORDER BY
                            points DESC, created_at ASC
                    )
                    AS allRanking
                    WHERE allRanking.user_id = " . $user->getId();

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $ranking = $result[0]['rank'];

        $query = "SELECT
                        COUNT(user_game.id) AS userGames
                    FROM
                        user_game
                    RIGHT JOIN
                            game
                        ON
                            user_game.game_id = game.id
                    WHERE
                        user_game.user_id = " . $user->getId() . " AND
                        game.status_id = '" . GameStatus::CLOSED . "';";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $games = $result[0]['userGames'];

        $query = "SELECT
                        COUNT(*) AS totalGames
                    FROM (
                        SELECT
                            COUNT(user_game.id) AS gamesPlayed
                        FROM
                            user_game
                        RIGHT JOIN
                                game
                            ON
                                user_game.game_id = game.id
                        WHERE
                            (user_game.user_id = " . $this->getUser()->getId() . " OR
                                user_game.user_id = " . $user->getId() . ") AND
                            game.status_id = '" . GameStatus::CLOSED . "'
                        GROUP BY
                            user_game.game_id
                        HAVING
                            gamesPlayed = 2
                    ) gamesResult;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $result = $dbConn->fetchAll();
        $gamesPlayed = $result[0]['totalGames'];

        return new JsonResponse([
            'id'    => (int) $user->getId(),
            'email' => $user->getEmail(),
            'profile' => [
                'id'               => (int) $profile->getId(),
                'first_name'       => $profile->getFirstName(),
                'last_name'        => $profile->getLastName(),
                'image_url'        => $profile->getImageUrl(),
                'city'             => $profile->getCity(),
                'country'          => $profile->getCountry(),
                'points'           => (int) $profile->getPoints(),
                'ranking'          => (int) $ranking,
                'total_players'    => (int) $profilesNr,
                'games'            => (int) $games,
                'games_against_me' => (int) $gamesPlayed
            ]
        ]);
    }

   /**
     *
     * @Put("/users")
     *
     * @return JsonResponse
     */
    public function updateUserAction(Request $request)
    {
        $user = $this->getUser();

        $firebaseToken = $request->get('firebaseToken');
        $deviceType    = $request->get('deviceType');

        $now = new DateTime();

        $user->setFirebaseToken($firebaseToken);
        if (isset($deviceType)) {
            $user->setDeviceType($deviceType);
        }

        $user->setUpdatedAt($now);

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();

        return new JsonResponse(array('user' => 'UPDATED'));
    }

    /**
     *
     * @Patch("/users/xplevel")
     *
     * @return JsonResponse
     */
    public function updateUserXpLevelAction(Request $request)
    {
        $xpLevelId = $request->get('xp_level_id');
        $response = $this->forward('AppBundle\Controller\Api\UserController::patchUserAction',
                                        ['xp_level_id' => $xpLevelId]
                                    );

        return $response;
    }

    /**
     *
     * @Patch("/users/onboarded")
     *
     * @return JsonResponse
     */
    public function updateOnboardedAction(Request $request)
    {
        $request->attributes->set('onboarded', true);

        $response = $this->forward(
            'AppBundle\Controller\Api\UserController::patchUserAction',
            ['request' => $request]
        );

        return $response;
    }

    /**
     *
     * @Patch("/users")
     *
     * @return JsonResponse
     */
    public function patchUserAction(Request $request)
    {
        $user = $this->getUser();

        $xpLevelId = $request->get('xp_level_id');

        if (isset($xpLevelId)) {
            $xpLevel = $this->getXpLevelRepo()->findOneById($xpLevelId);

            if (is_null($xpLevel)) {
                return new Response('Wrong status.', Response::HTTP_NOT_FOUND);
            }

            $user->setXpLevel($xpLevel);
        }

        $onboarded = $request->get('onboarded');

        if (isset($onboarded)) {
            $user->setOnboarded($onboarded);
        }

        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();

        return new JsonResponse('UPDATED');
    }
}

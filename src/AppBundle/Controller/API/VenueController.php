<?php

namespace AppBundle\Controller\API;

use AppBundle\Controller\API\FirebaseController;
use AppBundle\Entity\Availability;
use AppBundle\Entity\ChallengeStatus;
use AppBundle\Entity\GameStatus;
use AppBundle\Entity\WeekDay;
use DateInterval;
use DateTime;
use DatePeriod;
use Exception;
use FOS\RestBundle\Controller\Annotations\Get;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VenueController extends BaseController
{
    const VENUE_BOOKED_STATUS = 'Booked';
    const VENUE_FREE_STATUS   = 'Free';

    /**
     *
     * @Get("/venues")
     *
     * @return JsonResponse
     */
    public function venueAction(Request $request)
    {
        $venues = $this->getLocationRepo()->findAll();

        $venueList = [];

        foreach ($venues as $venue) {
            $venueList[] = array (
                'id'      => (int) $venue->getId(),
                'name'    => $venue->getName(),
                'country' => $venue->getCountry(),
                'city'    => $venue->getCity(),
                'address' => $venue->getAddress(),
                'image'   => 'https://cdn.royalcliff.info/images/fitz-club/04-Our-Services.jpg'
            );
        }

        return new JsonResponse($venueList);
    }

    /**
     *
     * @Get("/venues/{id}/availability"), requirements={"id" = "\d+"})
     *
     * @return JsonResponse
     */
    public function venueAvailabilityAction(Request $request, $id)
    {
        $userId = $this->getUser()->getId();
        $venue = $this->getLocationRepo()->findOneById($id);

        if (NULL === $venue) {
            return new Response('Venue not found.', Response::HTTP_NOT_FOUND);
        }

        $periods = new DatePeriod(new DateTime, new DateInterval('P1D'), 6);
        $days    = iterator_to_array($periods);

        $venueSchedule = json_decode($this->forward('AppBundle\Controller\API\VenueController::venueScheduleAction',
                                [
                                    'request' => $request,
                                    'id'      => $id
                                ]
                            )->getContent());

        $scheduleDayHours = [];
        foreach ($venueSchedule as $dayId=>$dayVenueSchedule) {
            $scheduleDayHours[$dayId] = [
                'start' => $dayVenueSchedule->start,
                'end'   => $dayVenueSchedule->end
            ];
        }

        $profileQuery = "SELECT
                                week_day_id,
                                game_id,
                                start,
                                end
                            FROM
                                venue_availability
                            WHERE
                                venue_id = " . $id;

        $dbConn = $this->getEntityManager()->getConnection()->prepare($profileQuery);
        $dbConn->execute();
        $intervals = $dbConn->fetchAll();

        $unavailableDayHours = [];

        foreach ($intervals as $interval) {
            $unavailableDayHours[$interval['week_day_id']][] = [
                'start'       => $interval['start'],
                'end'         => $interval['end']
            ];
        }

        $externalVenueUnavailableHours = $this->getVenueOutsideSchedule();

        $availability = [];
        foreach($days as $day) {

            $venueDayUserAvailabilityHours = self::getVenueUserAvailabilityHours($day, $userId);

            $dayId = date('N', strtotime($day->format('Y-m-d')));

            $intervals = [];
            if (isset($unavailableDayHours[$dayId])) {
                $intervals = $unavailableDayHours[$dayId];
            }

            $schedule = [];
            if (isset($scheduleDayHours[$dayId])) {
                $schedule = $scheduleDayHours[$dayId];
            }

            $venueRangeHours = range($schedule['start'], $schedule['end']);

            $unavailableHours = [];

            foreach ($venueRangeHours as $venueRangeHour) {

                $disableHour = true;

                if (isset($venueDayUserAvailabilityHours[$dayId])){
                    foreach($venueDayUserAvailabilityHours[$dayId] as $venueDayUserAvailabilityHoursInterval) {
                        if ($venueRangeHour >= $venueDayUserAvailabilityHoursInterval['start'] &&
                            $venueRangeHour <= $venueDayUserAvailabilityHoursInterval['end'])
                        {
                            $disableHour = false;
                        }
                    }
                }

                if ($disableHour){
                    array_push($unavailableHours, $venueRangeHour);
                }
            }

            if (isset($externalVenueUnavailableHours[$dayId])) {
                $externalVenueUnavailableDayHours = $externalVenueUnavailableHours[$dayId];

                foreach ($externalVenueUnavailableDayHours as $hour) {
                    if (!in_array((int)$hour, $unavailableHours)) {
                        array_push($unavailableHours, (int) $hour);
                    }
                }
            }

            $availability[] = [
                'date'                      => strtotime($day->format('Y-m-d')),
                'intervals'                 => $intervals,
                'players_unavailable_hours' => $unavailableHours,
                'schedule'                  => $schedule
            ];
        }
        return new JsonResponse($availability);
    }

    /**
     *
     * @Get("/venues/{id}/schedule"), requirements={"id" = "\d+"})
     *
     * @return JsonResponse
     */
    public function venueScheduleAction(Request $request, $id)
    {
        $profileQuery = "SELECT
                                week_day_id,
                                start,
                                end
                            FROM
                                venue_schedule
                            WHERE
                                venue_id = " . $id;

        $dbConn = $this->getEntityManager()->getConnection()->prepare($profileQuery);
        $dbConn->execute();
        $intervals = $dbConn->fetchAll();

        $schedule = [];

        foreach ($intervals as $interval) {
            $schedule[$interval['week_day_id']] = [
                'start'       => $interval['start'],
                'end'         => $interval['end']
            ];
        }

        return new JsonResponse($schedule);
    }

    /**
     *
     * @Get("/venues/get-reservation")
     *
     * @return JsonResponse
     */
    public function getVenueOutsideScheduleAction()
    {
        $client = new GuzzleClient();

        $response       = $client->request('GET', 'http://squashcluj.ro/1on1-api/getcalendar.php');
        $responseStatus = $response->getStatusCode();

        $schedule = [];

        if (Response::HTTP_OK === $responseStatus) {
            $weekSchedule = json_decode($response->getBody());

            $stringSchedule             = $this->getOnlyBookedHoursSchedule($weekSchedule);
            $allCourtsSchedule          = $this->getHoursSchedule($stringSchedule);
            $scheduleBookedForAllCourts = $this->getAllCourtsBookedHours($allCourtsSchedule);
        }

        return new JsonResponse($scheduleBookedForAllCourts);
    }

    /**
     *
     * @Get("/venues/week-unavailable-hours")
     *
     * @return JsonResponse
     */
    public function getVenueWeekUnavailableHoursAction()
    {
        $client = new GuzzleClient();

        $response       = $client->request('GET', 'http://squashcluj.ro/1on1-api/getcalendar.php');
        $responseStatus = $response->getStatusCode();

        $schedule = [];

        if (Response::HTTP_OK === $responseStatus) {
            $weekSchedule = json_decode($response->getBody());

            $stringSchedule             = $this->getOnlyBookedHoursSchedule($weekSchedule);
            $allCourtsSchedule          = $this->getHoursSchedule($stringSchedule);
            $scheduleBookedForAllCourts = $this->getAllCourtsBookedHours($allCourtsSchedule);
        }

        $bookedHours = [];
        foreach ($scheduleBookedForAllCourts as $dayId => $hours) {
            $bookedHours[] = [
                'day_id' => $dayId,
                'hours'  => $hours
            ];
        }

        return new JsonResponse($bookedHours);
    }

    public function isHourAvailable($dayId, $hour = null)
    {
        $bookedHours = self::getVenueOutsideSchedule();
        $hour = gmdate('H:i:s', floor($hour * 3600));
        $stringHour = date('G.i', strtotime($hour));

        $client = new GuzzleClient();

        $response       = $client->request('GET', 'http://squashcluj.ro/1on1-api/getcalendar.php');
        $responseStatus = $response->getStatusCode();

        if (Response::HTTP_OK === $responseStatus) {
            $weekSchedule = json_decode($response->getBody());

            $stringSchedule             = self::getOnlyBookedHoursSchedule($weekSchedule);
            $allCourtsSchedule          = self::getHoursSchedule($stringSchedule);

            if (!isset($allCourtsSchedule[$dayId])) {
                return true;
            }

            $hoursCount = array_count_values($allCourtsSchedule[$dayId]);

            if (isset($hoursCount[$stringHour]) && $hoursCount[$stringHour] === 2) {
                return false;
            }
        }

        return true;
    }

    public function getOnlyBookedHoursSchedule($weekSchedule = null)
    {
        if (null === $weekSchedule) {
            return [];
        }

        foreach ($weekSchedule as $day => $hours) {
            $date  = DateTime::createFromFormat('d-m-Y', $day);
            $dayId = date('N', strtotime($date->format('Y-m-d')));

            foreach ($hours as $hour => $status) {
                if($status !== self::VENUE_BOOKED_STATUS) {
                    unset($weekSchedule->$day->$hour);
                }
            }
        }

        return $weekSchedule;
    }

    public function getAllCourtsBookedRowHours($schedule = null)
    {
        if (null === $schedule) {
            return [];
        }

        $finalBookedHours = [];

        foreach ($schedule as $dayId => $hours) {
            foreach ($hours as &$hour) {
                $hour = (string)((int)$hour) . '.00';
            }

            $hoursCount = array_count_values($hours);

            foreach ($hoursCount as $hourCounted => $countedValue) {
                if ($countedValue === 4) {
                    $finalBookedHours[$dayId][] = $hourCounted;
                }
            }
        }

        return $finalBookedHours;
    }

    public function getAllCourtsBookedHours($schedule = null)
    {
        if (null === $schedule) {
            return [];
        }

        $finalBookedHours = [];

        foreach ($schedule as $dayId => $hours) {
            $hoursCount = array_count_values($hours);

            foreach ($hoursCount as $hourCounted => $countedValue) {
                if ($countedValue === 2) {
                    $finalBookedHours[$dayId][] = $hourCounted;
                }
            }
        }
        return $finalBookedHours;
    }

    public function getHoursSchedule($weekSchedule = null)
    {
        if (null === $weekSchedule) {
            return [];
        }

        foreach ($weekSchedule as $day => $hours) {
            $date  = DateTime::createFromFormat('d-m-Y', $day);
            $dayId = date('N', strtotime($date->format('Y-m-d')));

            foreach ($hours as $hour => $status) {
                $hourString = substr($hour, 0, 4);

                if($status === self::VENUE_BOOKED_STATUS) {
                    $schedule[$dayId][] = date('G.i', strtotime($hourString));
                }
            }
        }

        return $schedule;
    }

    public function getVenueOutsideSchedule()
    {
        $client = new GuzzleClient();

        $response       = $client->request('GET', 'http://squashcluj.ro/1on1-api/getcalendar.php');
        $responseStatus = $response->getStatusCode();

        $schedule = [];

        if (Response::HTTP_OK === $responseStatus) {
            $weekSchedule = json_decode($response->getBody());

            $stringSchedule             = self::getOnlyBookedHoursSchedule($weekSchedule);
            $allCourtsSchedule          = self::getHoursSchedule($stringSchedule);
            $scheduleBookedForAllCourts = self::getAllCourtsBookedRowHours($allCourtsSchedule);
        }

        return $scheduleBookedForAllCourts;
    }

    /**
     *
     * @Get("/venues/make-reservation")
     *
     * @return JsonResponse
     */
    public function makeReservationAction()
    {
        $challenge = $this->getChallengeRepo()->findOneById(1);
        $booked = self::bookCourt($challenge->getDate()->format('Y-m-d'), $challenge->getHour());

        $response = [
            'booked' => $booked
        ];

        return new JsonResponse($response);
    }

    public function bookCourt($challengeDate, $challengeHour, $status = self::VENUE_BOOKED_STATUS)
    {
        $challengeHourValue    = date('G', strtotime($challengeHour));
        $challengeMinutesValue = gmdate('i', round(($challengeHour - floor($challengeHour)) * 3600));

        $hourToSend = $challengeHourValue . $challengeMinutesValue;

        $courtIds = [
            '',
            'x'
        ];

        if ($challengeMinutesValue === "30") {
            $courtIds = [
                'a',
                'b'
            ];
        }

        $booked = false;

        foreach ($courtIds as $courtId) {
            $hour = $hourToSend . $courtId;
            $bookingResponse = self::makeExternalVenueBooking($challengeDate, $hour, $status);

            if (
                null !== $bookingResponse &&
                Response::HTTP_OK === $bookingResponse->getStatusCode()
            ) {
                $booked = true;
                break;
            }

        }

        return $booked;
    }

    private function makeExternalVenueBooking($date, $hour, $status = self::VENUE_BOOKED_STATUS)
    {
        $params = [
            'hour'   => $hour,
            'status' => $status,
            'date'   => $date
        ];

        $response = true;

        try {
            $client  = new GuzzleClient();
            $request = new GuzzleRequest('POST', 'http://squashcluj.ro/1on1-api/savecalendar.php');

            $response = $client->send($request, [
                    'form_params' => $params
                ]);
        } catch (RequestException $e) {
            $response = $e->getResponse();
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        return $response;
    }

    private function getVenueUserAvailabilityHours($day, $userId)
    {
        $SQLDate = $day->format('Y-m-d');
        $dayId   = date('N', strtotime($SQLDate));

        $query = "SELECT
                        week_day_id, start, end
                    FROM
                        availability
                    WHERE
                            user_id NOT IN (SELECT
                                                user_id
                                            FROM
                                                user_game
                                            JOIN
                                                game ON user_game.game_id = game.id
                                            WHERE
                                                game.status_id = " . GameStatus::UPCOMING_GAME . "
                                            GROUP BY
                                                user_id)
                        AND
                            user_id NOT IN (SELECT
                                                ugo.user_id
                                            FROM
                                                user_game AS ug
                                            JOIN
                                                    challenge AS c
                                                ON
                                                    c.game_id = ug.game_id
                                            JOIN
                                                    user_game AS ugo
                                                ON
                                                    ugo.game_id = ug.game_id
                                            WHERE
                                                    ug.user_id = " . $userId . "
                                                AND
                                                    c.status_id = " . ChallengeStatus::NOT_CONFIRMED . "
                                                AND
                                                    DATE(c.date) = '" . $SQLDate . "')
                        AND
                            week_day_id = " . $dayId . "
                    GROUP BY
                        week_day_id, start, end;";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($query);
        $dbConn->execute();
        $venueUserAvailabilityHours = $dbConn->fetchAll();

        $venueDayUserAvailabilityHours = [];
        foreach($venueUserAvailabilityHours as $venueUserAvailabilityHour) {
            $venueDayUserAvailabilityHours[$venueUserAvailabilityHour['week_day_id']][] = [
                'start' => $venueUserAvailabilityHour['start'],
                'end' => $venueUserAvailabilityHour['end']
            ];
        }

        return $venueDayUserAvailabilityHours;
    }
}

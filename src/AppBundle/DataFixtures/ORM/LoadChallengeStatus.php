<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\ChallengeStatus;

class LoadChallengeStatus implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $statuses = [
            'NOT_CONFIRMED',
            'ACCEPTED',
            'REJECTED',
            'DELETED',
            'CLOSED'
        ];

        foreach ($statuses as $status) {
            $challengeStatus = new ChallengeStatus();
                $challengeStatus->setName($status);
            $manager->persist($challengeStatus);
        }

        $manager->flush();
    }
}

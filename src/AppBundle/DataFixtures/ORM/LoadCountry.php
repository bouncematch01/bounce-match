<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Country;
use AppBundle\Entity\City;

class LoadCountryAndCityStatus implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $country = new Country();
        $country->setName('Romania');
        $manager->persist($country);

        $manager->flush();

        #TODO use OrderedFixtureInterface
        $city = new City();
        $city->setCountry($country);
        $city->setName('Cluj-Napoca');
        $manager->persist($city);

        $manager->flush();

    }
}

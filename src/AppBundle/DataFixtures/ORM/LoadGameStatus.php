<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\GameStatus;

class LoadGameStatus implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $statuses = [
            'OPENED',
            'IN_PROGRESS',
            'CLOSED',
            'DELETED',
            'CHALLENGE',
            'UPCOMING_GAME',
            'CANCELED',
            'NOT_CONFIRMED',
            'REJECTED'
        ];

        foreach ($statuses as $status) {
            $gameStatus = new GameStatus();
                $gameStatus->setName($status);
            $manager->persist($gameStatus);
        }

        $manager->flush();
    }
}

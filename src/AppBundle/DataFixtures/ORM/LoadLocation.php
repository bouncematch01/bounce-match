<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Location;

class LoadLocation implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $location = new Location();
            $location->setName('Squash Club Cluj Napoca');
            $location->setCountry('Romania');
            $location->setCity('Cluj-Napoca');
            $location->setAddress('Strada Serpuitoare FN(vis a vis nr. 39)');
        $manager->persist($location);

        $manager->flush();
    }
}

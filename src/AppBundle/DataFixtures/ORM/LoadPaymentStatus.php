<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\PaymentStatus;

class LoadPaymentStatus implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $statuses = [
            'OPENED',
            'USED',
            'DELETED'
        ];

        foreach ($statuses as $status) {
            $paymentStatus = new PaymentStatus();
                $paymentStatus->setName($status);
            $manager->persist($paymentStatus);
        }

        $manager->flush();
    }
}

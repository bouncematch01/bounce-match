<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\PaymentTransactionStatus;

class LoadPaymentTransactionStatus implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $statuses = [
            'NEW',
            'CONFIRMED',
            'PENDING',
            'OPEN',
            'CANCELED',
            'REFUNDED',
            'REJECTED'
        ];

        foreach ($statuses as $status) {
            $paymentTransactionStatus = new PaymentTransactionStatus();
                $paymentTransactionStatus->setName($status);
            $manager->persist($paymentTransactionStatus);
        }

        $manager->flush();
    }
}

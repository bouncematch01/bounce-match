<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\UserStatus;

class LoadUserStatus implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $statuses = [
            'ACTIVE',
            'DELETED',
            'ONBOARDING'
        ];

        foreach ($statuses as $status) {
            $userStatus = new UserStatus();
                $userStatus->setName($status);
            $manager->persist($userStatus);
        }

        $manager->flush();
    }
}

<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\WeekDay;

class LoadWeekDay implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $days = [
            'MONDAY',
            'TUESDAY',
            'WEDNESDAY',
            'THURSDAY',
            'FRIDAY',
            'SATURDAY',
            'SUNDAY'
        ];

        foreach ($days as $day) {
            $weekDay = new WeekDay();
                $weekDay->setName($day);
            $manager->persist($weekDay);
        }

        $manager->flush();
    }
}

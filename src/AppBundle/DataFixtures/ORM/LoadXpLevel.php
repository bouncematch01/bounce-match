<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\XpLevel;

class LoadXpLevel implements FixtureInterface
{
    const DEFAULT_POINTS = 1200;

    public function load(ObjectManager $manager)
    {
        $levels = [
            'NEW'          => self::DEFAULT_POINTS,
            'BEGINNER'     => self::DEFAULT_POINTS,
            'INTERMEDIATE' => self::DEFAULT_POINTS,
            'ADVANCED'     => self::DEFAULT_POINTS
        ];

        foreach ($levels as $levelName => $points) {
            $level = new XpLevel();
                $level->setName($levelName);
                $level->setPoints($points);
            $manager->persist($level);
        }

        $manager->flush();
    }
}

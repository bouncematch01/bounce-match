<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User as User;
use AppBundle\Entity\WeekDay as WeekDay;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Availability
 *
 * @ORM\Table(name="availability", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="FK_AVAILABILITY_USER_idx", columns={"user_id"}), @ORM\Index(name="FK_AVAILABILITY_WEEKDAYS_idx", columns={"week_day_id"})})
 * @ORM\Entity
 */
class Availability
{
    /**
     * @var string
     *
     * @ORM\Column(name="start", type="decimal", precision=4, scale=1, nullable=false)
     */
    private $start;

    /**
     * @var string
     *
     * @ORM\Column(name="end", type="decimal", precision=4, scale=1, nullable=false)
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\WeekDay
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\WeekDay")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="week_day_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $weekDay;

    public function __construct()
    {
        $now = new DateTime();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

   /**
     * @param $start
     *
     * @return Availability
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * @return decimal
     */
    public function getStart()
    {
        return $this->start;
    }

   /**
     * @param $end
     *
     * @return Availability
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * @return decimal
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return Availability
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $user
     *
     * @return Availability
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param WeekDay $weekDay
     *
     * @return Availability
     */
    public function setWeekDay(WeekDay $weekDay = null)
    {
        $this->weekDay = $weekDay;

        return $this;
    }

    /**
     * @return WeekDay
     */
    public function getWeekDay()
    {
        return $this->weekDay;
    }
}

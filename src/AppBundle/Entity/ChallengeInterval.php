<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Challenge as Challenge;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ChallengeInterval
 *
 * @ORM\Table(name="challenge_interval", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="FK_CHALLENGE_INTERVAL_CHALLENGE_idx", columns={"challenge_id"})})
 * @ORM\Entity
 */
class ChallengeInterval
{
    /**
     * @var string
     *
     * @ORM\Column(name="start", type="decimal", precision=4, scale=1, nullable=false)
     */
    private $start;

    /**
     * @var string
     *
     * @ORM\Column(name="end", type="decimal", precision=4, scale=1, nullable=false)
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Challenge
     *
     * @ORM\ManyToOne(targetEntity="Challenge")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="challenge_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $challenge;


    public function __construct()
    {
        $now = new DateTime();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

   /**
     * @param $start
     *
     * @return ChallengeInterval
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * @return decimal
     */
    public function getStart()
    {
        return $this->start;
    }

   /**
     * @param $end
     *
     * @return ChallengeInterval
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * @return decimal
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return ChallengeInterval
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Challenge $challenge
     *
     * @return ChallengeInterval
     */
    public function setChallenge(Challenge $challenge = null)
    {
        $this->challenge = $challenge;

        return $this;
    }

    /**
     * @return Challenge
     */
    public function getChallenge()
    {
        return $this->challenge;
    }
}

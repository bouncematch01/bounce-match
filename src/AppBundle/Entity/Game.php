<?php

namespace AppBundle\Entity;

use AppBundle\Entity\GameStatus as GameStatus;
use AppBundle\Entity\Location as Location;
use AppBundle\Entity\User as User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Game
 *
 * @ORM\Table(name="game", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="FK_LOCATION_idx", columns={"location_id"}), @ORM\Index(name="FK_GAME_GAME_STATUS_idx", columns={"status_id"}), @ORM\Index(name="FK_GAME_CREATED_BY_USER_idx", columns={"created_by_user_id"}), @ORM\Index(name="FK_GAME_INSERT_SCORE_USER_idx", columns={"score_inserted_by_user_id"})})
 * @ORM\Entity
 */
class Game
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Location
     *
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="location_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $location;

    /**
     * @var GameStatus
     *
     * @ORM\ManyToOne(targetEntity="GameStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $status;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $user;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="score_inserted_by_user_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $scoreSavedByUser;

    public function __construct()
    {
        $now = new DateTime();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Game
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param Location $location
     *
     * @return Game
     */
    public function setLocation(Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set status
     *
     * @param GameStatus $status
     *
     * @return Game
     */
    public function setStatus(GameStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return GameStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserGame
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set scoreSavedByUser
     *
     * @param User $scoreSavedByUser
     *
     * @return UserGame
     */
    public function setScoreSavedByUser(User $scoreSavedByUser = null)
    {
        $this->scoreSavedByUser = $scoreSavedByUser;

        return $this;
    }

    /**
     * Get scoreSavedByUser
     *
     * @return User
     */
    public function getScoreSavedByUser()
    {
        return $this->scoreSavedByUser;
    }

}

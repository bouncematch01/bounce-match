<?php

namespace AppBundle\Entity;

use AppBundle\Entity\UserStatus as UserStatus;
use AppBundle\Entity\XpLevel as XpLevel;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="FK_USER_USER_STATUS_idx", columns={"status_id"}), @ORM\Index(name="FK_PROFILE_XP_LEVEL_idx", columns={"xp_level_id"})})
 * @ORM\Entity
 */
class User extends BaseUser
{
    const DEFAULT_KFACTOR = 40;

    /**
     * {@inheritdoc}
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=false)
     */
    private $facebookAccessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", length=32, nullable=false)
     */
    private $facebookId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_type", type="string", length=32, nullable=true)
     */
    private $deviceType;

    /**
     * @var string
     *
     * @ORM\Column(name="firebase_token", type="string", length=255, nullable=true)
     */
    private $firebaseToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var UserStatus
     *
     * @ORM\ManyToOne(targetEntity="UserStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="onboarded", type="boolean", nullable=false, options={"default" : false})
     */
    private $onboarded;

    /**
     * @var XpLevel
     *
     * @ORM\ManyToOne(targetEntity="XpLevel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="xp_level_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $xpLevel;

   /**
     * @var integer
     *
     * @ORM\Column(name="kfactor", type="integer", nullable=false, options={"default" : User::DEFAULT_KFACTOR})
     */
    private $kFactor = self::DEFAULT_KFACTOR;

    public function __construct()
    {
        parent::__construct();

        $now = new DateTime();
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    /**
     * @param string $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * @return string
     */
    public function getFirebaseToken()
    {
        return $this->firebaseToken;
    }

    /**
     * @param string $firebaseToken
     *
     * @return User
     */
    public function setFirebaseToken($firebaseToken)
    {
        $this->firebaseToken = $firebaseToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceType()
    {
        return $this->deviceType;
    }

    /**
     * @param string $deviceType
     *
     * @return User
     */
    public function setDeviceType($deviceType)
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param UserStatus $status
     *
     * @return User
     */
    public function setStatus(UserStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return UserStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $onboarded
     *
     * @return User
     */
    public function setOnboarded($onboarded)
    {
        $this->onboarded = $onboarded;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getOnboarded()
    {
        return $this->onboarded;
    }

    /**
     * @param XpLevel $xpLevel
     *
     * @return Profile
     */
    public function setXpLevel(XpLevel $xpLevel = null)
    {
        $this->xpLevel = $xpLevel;

        return $this;
    }

    /**
     * @return XpLevel
     */
    public function getXpLevel()
    {
        return $this->xpLevel;
    }

    /**
     * @param integer $kFactor
     *
     * @return User
     */
    public function setKFactor($kFactor)
    {
        $this->kFactor = $kFactor;

        return $this;
    }

    /**
     * @return integer
     */
    public function getkFactor()
    {
        return $this->kFactor;
    }
}

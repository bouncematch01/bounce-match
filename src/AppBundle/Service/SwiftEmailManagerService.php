<?php

namespace AppBundle\Service;

use Swift_Message as SwiftMessage;

class SwiftEmailManagerService
{
    /**
     * @var string
     */
    private $mailer;

    public function __construct($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Generate & send a reservation email.
     *
     * @param  string $hour
     * @param  string $date
     * @param  string $name
     *
     * @return integer The number of successful recipients. Can be 0 which indicates failure.
     */
    public function sendReservationEmail($hour, $date, $name)
    {
        $subject = 'Rezervare noua de pe aplicatia BounceMatch.';
        $body    = 'Rezervare noua facuta de ' .
                    $name . ' pe data de ' .
                    $date . ' la ora ' .
                    $hour . '.';
        $from    = ['1on1testplayer@gmail.com' => 'Bounce Match App'];
        $to      = ['ovidiumaritan@yahoo.com', 'cristi.anghel@qubiz.com'];

        return self::sendEmail($subject, $body, $from, $to);
    }

    /**
     * Generate & send a cancellation email.
     *
     * @param  string $hour
     * @param  string $date
     * @param  string $name
     *
     * @return integer The number of successful recipients. Can be 0 which indicates failure.
     */
    public function sendCancellationEmail($hour, $date, $name)
    {
        $subject = 'Rezervare anulata de pe aplicatia BounceMatch.';
        $body    = 'Rezervarea facuta de ' .
                    $name . ' pe data de ' .
                    $date . ' la ora ' .
                    $hour . '.';
        $from    = ['1on1testplayer@gmail.com' => 'Bounce Match App'];
        $to      = ['ovidiumaritan@yahoo.com', 'cristi.anghel@qubiz.com'];

        return self::sendEmail($subject, $body, $from, $to);
    }

    /**
     * Generate & send an email.
     *
     * @param  string $body
     * @param  string $subject
     * @param  array $to
     * @param  array $from
     *
     * @return integer The number of successful recipients. Can be 0 which indicates failure.
     */
    private function sendEmail($subject, $body, array $from, array $to)
    {
        $message = SwiftMessage::newInstance() ->setSubject($subject)
                                                ->setFrom($from)
                                                ->setTo($to)
                                                ->setBody($body, 'text/html');

        return $this->mailer->send($message);
    }
}

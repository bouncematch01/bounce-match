<?php

namespace Qubiz\BookingListBundle\Controller;

use AppBundle\Entity\ChallengeStatus;
use AppBundle\Entity\GameStatus;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/booking")
 */
class BookingController extends BaseController
{

    /**
    * @Route("/list", name="list")
    * @Method("GET")
    */
    public function paymentReturnAction(Request $request)
    {
        $bookingsQuery = "SELECT
                                g.id as gameId,
                                l.name as venueName,
                                CONCAT(p.first_name, ' ', p.last_name) as playerName,
                                c.date as bookingDate,
                                c.hour as bookingHour
                            FROM
                                game as g
                            JOIN
                                    user_game as ug
                                ON
                                    ug.game_id = g.id
                            LEFT JOIN
                                    profile as p
                                ON
                                    p.user_id = g.created_by_user_id
                            JOIN
                                    location as l
                                ON
                                    l.id = g.location_id
                            LEFT JOIN
                                    challenge as c
                                ON
                                    c.game_id = g.id
                            WHERE
                                g.status_id IN (" .
                                    GameStatus::CLOSED . "," .
                                    GameStatus::UPCOMING_GAME . "," .
                                    GameStatus::NOT_CONFIRMED .
                                ")
                            GROUP BY
                                g.id, p.last_name, p.first_name, c.date, c.hour
                            ORDER BY
                                c.date DESC, c.hour DESC; ";

        $dbConn = $this->getEntityManager()->getConnection()->prepare($bookingsQuery);
        $dbConn->execute();
        $bookings = $dbConn->fetchAll();

        $bookingList = [];

        foreach ($bookings as $booking) {
            $bookingList[] = [
                'venue_name'    => $booking['venueName'],
                'player_name'   => $booking['playerName'],
                'booking_date'  => $booking['bookingDate'],
                'booking_hour'  => $booking['bookingHour']
            ];
        }

        return $this->render('QubizBookingListBundle:Default:list.html.twig', ['bookingList' => $bookingList]);
    }
}

<?php

namespace Qubiz\EloSystemBundle\Calculus;

class EloRating
{
    const KFACTOR_NO_GAMES         = 40;
    const KFACTOR_BELOW_MAX_POINTS = 32;
    const KFACTOR_ABOVE_MAX_POINTS = 16;

    const KFACTOR_GAMES_LIMIT  = 10;
    const KFACTOR_POINTS_LIMIT = 2000;

    private $myRating;
    private $opponentRating;
    private $myScore;
    private $opponentScore;
    private $myExpectedScore;
    private $opponentExpectedScore;
    private $myNewScore;
    private $opponentNewScore;
    private $kFactor;


    /**
     * Set new input data.
     *
     * @param int Current rating of A
     * @param int Current rating of B
     * @param int Score of A
     * @param int Score of B
     */
    public function setNewSettings($ratingA, $ratingB, $scoreA, $scoreB)
    {
        $this->myRating                 = $ratingA;
        $this->opponentRating           = $ratingB;
        $this->calculateScore($scoreA, $scoreB);

        $expectedScores                 = $this->calculateExpectedScores($this->myRating, $this->opponentRating);
        $this->myExpectedScore          = $expectedScores['myScore'];
        $this->opponentExpectedScore    = $expectedScores['opponentScore'];

        $newRatings                     = $this->calculateNewRatings(
                                            $this->myRating,
                                            $this->opponentRating,
                                            $this->myExpectedScore,
                                            $this->opponentExpectedScore,
                                            $this->myScore,
                                            $this->opponentScore
                                        );

        $this->myNewScore               = $newRatings['myRating'];
        $this->opponentNewScore         = $newRatings['opponentRating'];
    }

    /**
     * Retrieve the calculated data.
     * @param int Current rating of A
     * @param int Current rating of B
     * @param int Score of A
     * @param int Score of B
     *
     * @return Array An array containing the new ratings for A and B.
     */
    public function getNewRatings($ratingA, $ratingB, $scoreA, $scoreB, $kFactor)
    {
        $this->myRating       = $ratingA;
        $this->opponentRating = $ratingB;
        $this->kFactor        = $kFactor;
        $this->calculateScore($scoreA, $scoreB);

        $expectedScores                 = $this->calculateExpectedScores($this->myRating, $this->opponentRating);
        $this->myExpectedScore          = $expectedScores['myScore'];
        $this->opponentExpectedScore    = $expectedScores['opponentScore'];

        $newRatings                     = $this->calculateNewRatings(
                                            $this->myRating,
                                            $this->opponentRating,
                                            $this->myExpectedScore,
                                            $this->opponentExpectedScore,
                                            $this->myScore,
                                            $this->opponentScore
                                        );
        $this->myNewScore               = $newRatings['myRating'];
        $this->opponentNewScore         = $newRatings['opponentRating'];

        return array (
            'myRating'          => $this->myNewScore,
            'opponentRating'    => $this->opponentNewScore
        );
    }

    public function getPointsNeededForChancePercentage($rating, $percentage = 50)
    {
        $chanceToWin = 50;
        $opponentRating = $rating;
        $chanceToWin = 0;

        while ($chanceToWin != $percentage) {
            if ($percentage < 50) {
                $opponentRating++;
            } else {
                $opponentRating--;
            }

            $matchMakingInfo = $this->getMatchMakingInfo($rating, $opponentRating);

            $chanceToWin = $matchMakingInfo['chanceToWin'];
        }

        return $opponentRating;
    }

    /**
     * @param int Current rating of A
     * @param int Current rating of B
     */
    public function getMatchMakingInfo($ratingA = 1200, $ratingB = 1200, $kFactor)
    {
        $this->kFactor = $kFactor;
        $pointsResults = $this->calculateExpectedScores($ratingA, $ratingB);
        $chanceToWin   = (int) (round($pointsResults['myScore'], 2) * 100);

        $this->setNewSettings(
                $ratingA,
                $ratingB,
                $pointsResults['myScore'],
                $pointsResults['opponentScore']
            );

        $pointsGainedIfWin = $this->calculateNewRatings(
                $this->myRating,
                $this->opponentRating,
                $this->myExpectedScore,
                $this->opponentExpectedScore,
                1,
                0
            );

        $pointsToWin = abs($this->myRating - round($pointsGainedIfWin['myRating']));

        return [
            'chanceToWin' => $chanceToWin,
            'pointsToWin' => $pointsToWin
        ];
    }

    /**
     * @param int Current rating of A
     * @param int Current rating of B
     */
    public function getPointsToWin($ratingA = 1200, $ratingB = 1200)
    {
        return $this->calculateScore($ratingA, $ratingB);
    }

    private function setExpectedScore($myScore, $opponentScore)
    {
        $expectedScores = $this->calculateExpectedScores($myScore, $opponentScore);

        $this->myExpectedScore       = $expectedScores['myScore'];
        $this->opponentExpectedScore = $expectedScores['opponentScore'];

        return $this;
    }

    private function calculateScore($myScore, $opponentScore)
    {
        $myWin       = 0.5;
        $opponentWin = 0.5;
        if ($myScore > $opponentScore) {
            $myWin       = 1;
            $opponentWin = 0;
        } elseif($myScore < $opponentScore) {
            $myWin       = 0;
            $opponentWin = 1;
        }

        $this->myScore       = $myWin;
        $this->opponentScore = $opponentWin;
    }

    private function calculateExpectedScores($ratingA, $ratingB)
    {
        $expectedScoreA = 1 / (1 + (pow(10, ($ratingB - $ratingA) / 400)));
        $expectedScoreB = 1 / (1 + (pow(10, ($ratingA - $ratingB) / 400)));

        return array (
            'myScore'       => $expectedScoreA,
            'opponentScore' => $expectedScoreB
        );
    }

    private function calculateNewRatings($ratingA, $ratingB, $expectedA, $expectedB, $scoreA, $scoreB)
    {
        $newRatingA = $ratingA + ($this->kFactor * ($scoreA - $expectedA));
        $newRatingB = $ratingB + ($this->kFactor * ($scoreB - $expectedB));

        return array (
            'myRating'          => $newRatingA,
            'opponentRating'    => $newRatingB
        );
    }

    public function getNewKFactor(
        $gamesNr = self::KFACTOR_GAMES_LIMIT,
        $userKFactor = self::KFACTOR_NO_GAMES,
        $userPoints = self::KFACTOR_POINTS_LIMIT)
    {
        if (
            ($userKFactor === self::KFACTOR_ABOVE_MAX_POINTS || $userPoints >= self::KFACTOR_POINTS_LIMIT) &&
            $gamesNr >= self::KFACTOR_GAMES_LIMIT) {
            return self::KFACTOR_ABOVE_MAX_POINTS;
        } else if ($gamesNr >= self::KFACTOR_GAMES_LIMIT &&  $userPoints < self::KFACTOR_POINTS_LIMIT) {
            return self::KFACTOR_BELOW_MAX_POINTS;
        }

        return self::KFACTOR_NO_GAMES;
    }
}

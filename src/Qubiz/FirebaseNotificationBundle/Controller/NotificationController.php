<?php

namespace Qubiz\FirebaseNotificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class NotificationController extends Controller
{
    /**
     * Device type supported by the app
     *
     * @var string
     */
    const DEVICE_TYPE_ANDROID = 'android';

    /**
     * Device type supported by the app
     *
     * @var string
     */
    const DEVICE_TYPE_IOS = 'ios';

    private $apiAccessKey;
    private $firebaseToken;
    private $deviceType;

    private $fieldsToSend;

    public function __construct($apiAccessKey)
    {
        $this->apiAccessKey  = $apiAccessKey;
    }

    /**
     * @param  array $messageData
     * @return bool
     */
    public function sendNotification(
        $firebaseToken = null,
        $deviceType = self::DEVICE_TYPE_ANDROID,
        $messageData = null,
        $extraMessage = null)
    {
        $this->firebaseToken = $firebaseToken;
        $this->deviceType    = $deviceType;
        if (null === $this->firebaseToken) {
            return false;
        }

        $message = [
            'title'              => $messageData['title'],
            'body'               => $messageData['body'],
            'sound'              => 1,
            'notification_type'  => $messageData['notification_type']
        ];

        if (null !== $extraMessage) {
            foreach ($extraMessage as $key => $value) {
                $message[$key] = $value;
            }
        }

        $fields = [];

        if ($deviceType === self::DEVICE_TYPE_ANDROID) {
            $fields = [
                'to'   => $firebaseToken,
                'data' => $message
            ];
        } else {
            if (isset($messageData['subtitle'])) {
                $subtitle = $messageData['subtitle'];
            } else {
                $subtitle = $messageData['title'];
            }

            $message['click_action'] = $messageData['click_action'];
            $message['subtitle']     = $subtitle;

            $fields = [
                'to'           => $firebaseToken,
                'notification' => $message,
            ];
            #prev way I setted fields for ios
            // $fields = [
            //     'to'           => $firebaseToken,
            //     'notification' => $msg,
            //     'data'         => $data
            // ];
        }

        $this->fieldsToSend = $fields;

        return $this->connectAndSend();
    }

    private function connectAndSend()
    {
        $headers = [
            'Authorization: key=' . $this->apiAccessKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($this->fieldsToSend));
        $result = curl_exec($ch);
        curl_close($ch);

        return new JsonResponse($result);
    }
}

<?php

namespace Qubiz\MobilePayBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class BaseController extends Controller
{
    /**
     * @return Doctrine Entity Manager
     */
    protected function getEntityManager()
    {
        return $this->get('doctrine.orm.entity_manager');
    }

    /**
     * @return Game Repo
     */
    protected function getGameRepo()
    {
        return $this->get('app.repository.game');
    }

    /**
     * @return Payment Repo
     */
    protected function getPaymentRepo()
    {
        return $this->get('app.repository.payment');
    }

    /**
     * @return PaymentStatus Repo
     */
    protected function getPaymentStatusRepo()
    {
        return $this->get('app.repository.payment_status');
    }

    /**
     * @return PaymentTransactionStatus Repo
     */
    protected function getPaymentTransactionStatusRepo()
    {
        return $this->get('app.repository.payment_transaction_status');
    }

    /**
     * @return User Repo
     */
    protected function getUserRepo()
    {
        return $this->get('app.repository.user');
    }

    /**
     * @return Firebase notification Bundle
     */
    protected function getFirebaseNotification()
    {
        return $this->get('qubiz_firebase_notification.notification');
    }
}

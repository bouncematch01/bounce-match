<?php

namespace Qubiz\MobilePayBundle\Controller;


use AppBundle\Entity\Payment;
use AppBundle\Entity\PaymentStatus;
use AppBundle\Entity\PaymentTransactionStatus;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use stdClass;
use SoapClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Qubiz\MobilePayBundle\MobilePayResources\Request\Mobilpay_Payment_Request_Abstract;
use Qubiz\MobilePayBundle\MobilePayResources\Request\Mobilpay_Payment_Request_Card as Mobilpay_Payment_Request_Card;
use Qubiz\MobilePayBundle\MobilePayResources\Request\Mobilpay_Payment_Request_Notify;
use Qubiz\MobilePayBundle\MobilePayResources\Mobilpay_Payment_Invoice;
use Qubiz\MobilePayBundle\MobilePayResources\Mobilpay_Payment_Address;

/**
 * @Route("/payment")
 */
class PaymentController extends BaseController
{
    private $apiAccessKey;
    private $apiUserName;
    private $apiPassword;

    public function __construct($apiAccessKey, $apiUserName, $apiPassword)
    {
        $this->apiAccessKey = $apiAccessKey;
        $this->apiUserName  = $apiUserName;
        $this->apiPassword  = $apiPassword;
    }

    /**
    * @Route("/details", name="details")
    * @Method("GET")
    */
    public function paymentDetailsAction(Request $request)
    {
        return $this->render('QubizMobilePayBundle:Default:details.html.twig');
    }

    /**
    * @Route("/redirect", name="redirect")
    * @Method("GET")
    */
    public function paymentRedirectAction(Request $request)
    {
        $user = $this->getUser();
        $paymentUrl = 'http://sandboxsecure.mobilpay.ro/card3';

        #TODO - redone this -> create a service that's using FileLocator
        $kernel = $this->container->get('kernel');
        $x509FilePath = $kernel->locateResource(
            '@QubizMobilePayBundle/Resources/assets/sandbox.Y8MM-6J1C-67A4-SQTG-JEPU.public.cer'
        );

        try
        {
            #TODO: move hardcoded values to yml & check why is there a need for the next line
            $orderId = md5(uniqid(rand()));
            srand((double) microtime() * 1000000);
            $objPmReqCard             = new Mobilpay_Payment_Request_Card();
            $objPmReqCard->signature  = 'Y8MM-6J1C-67A4-SQTG-JEPU';
            $objPmReqCard->orderId    = $orderId;
            $objPmReqCard->confirmUrl = 'http://81.196.63.146/payment/confirm';
            $objPmReqCard->returnUrl  = 'http://81.196.63.146/payment/return';

            $objPmReqCard->invoice = new Mobilpay_Payment_Invoice();
            $objPmReqCard->invoice->currency = 'RON';
            $objPmReqCard->invoice->amount   = '20.00';
            $objPmReqCard->invoice->tokenId  = 'token_id';
            $objPmReqCard->invoice->details  = 'Plata cu card-ul prin mobilPay';

            #TODO - use those fileds later if needed for billing
            // $billingAddress              = new Mobilpay_Payment_Address();
            // $billingAddress->type        = $_POST['billing_type']; //should be "person"
            // $billingAddress->firstName   = $_POST['billing_first_name'];
            // $billingAddress->lastName    = $_POST['billing_last_name'];
            // $billingAddress->address     = $_POST['billing_address'];
            // $billingAddress->email       = $_POST['billing_email'];
            // $billingAddress->mobilePhone = $_POST['billing_mobile_phone'];
            // $objPmReqCard->invoice->setBillingAddress($billingAddress);

            $objPmReqCard->encrypt($x509FilePath);
        }
        catch(Exception $e)
        {

        }

        $paymentStatus = $this->getPaymentStatusRepo()->findOneById(
                PaymentStatus::OPENED
            );
        $paymentTransactionStatus = $this->getPaymentTransactionStatusRepo()->findOneById(
                PaymentTransactionStatus::NEW
            );

        $payment = new Payment();
            $payment->setStatus($paymentStatus);
            $payment->setTransactionStatus($paymentTransactionStatus);
            $payment->setUser($user);
            $payment->setOrderId($orderId);
        $this->getEntityManager()->persist($payment);

        $this->getEntityManager()->flush();

        $paymentInfo = [
            'paymentUrl' => $paymentUrl,
            'envKey'     => $objPmReqCard->getEnvKey(),
            'encData'    => $objPmReqCard->getEncData()
        ];

        return $this->render('QubizMobilePayBundle:Default:redirect.html.twig', $paymentInfo);
    }

    /**
    * @Route("/confirm", name="confirm")
    * @Method("POST")
    */
    public function paymentConfirmAction(Request $request)
    {
        $paymentStatusType            = PaymentStatus::OPENED;
        $paymentTransactionStatusType = PaymentTransactionStatus::NEW;

        $errorCode      = 0;
        $errorType      = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_NONE;
        $errorMessage   = '';

        $orderId = null;

        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'post') == 0)
        {
            if(isset($_POST['env_key']) && isset($_POST['data']))
            {
                #TODO - redone this -> create a service that's using FileLocator
                $kernel = $this->container->get('kernel');
                $privateKeyFilePath = $kernel->locateResource(
                    '@QubizMobilePayBundle/Resources/assets/sandbox.Y8MM-6J1C-67A4-SQTG-JEPUprivate.key'
                );

                try
                {
                    $objPmReq = Mobilpay_Payment_Request_Abstract::factoryFromEncrypted(
                        $_POST['env_key'], $_POST['data'], $privateKeyFilePath
                    );

                    $orderId = $objPmReq->orderId;
                    $rrn = $objPmReq->objPmNotify->rrn;
                    // action = status only if the associated error code is zero
                    if ($objPmReq->objPmNotify->errorCode == 0) {
                        switch($objPmReq->objPmNotify->action)
                        {
                            case 'confirmed':
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                $paymentTransactionStatusType = PaymentTransactionStatus::CONFIRMED;
                            break;
                            case 'confirmed_pending':
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                $paymentTransactionStatusType = PaymentTransactionStatus::PENDING;
                            break;
                            case 'paid_pending':
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                $paymentTransactionStatusType = PaymentTransactionStatus::PENDING;
                            break;
                            case 'paid':
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                $paymentTransactionStatusType = PaymentTransactionStatus::OPEN;
                            break;
                            case 'canceled':
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                $paymentTransactionStatusType = PaymentTransactionStatus::CANCELED;
                                $paymentStatusType            = PaymentStatus::USED;
                            break;
                            case 'credit':
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                $paymentTransactionStatusType = PaymentTransactionStatus::REFUNDED;
                                $paymentStatusType            = PaymentStatus::USED;
                            break;
                            default:
                                $paymentStatusType            = PaymentStatus::OPENED;
                                $paymentTransactionStatusType = PaymentTransactionStatus::NEW;

                                $errorType    = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                                $errorCode    = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_ACTION;
                                $errorMessage = 'mobilpay_refference_action paramaters is invalid';
                            break;
                        }
                    } else {
                        //update DB, SET status = "rejected"
                        $errorMessage = $objPmReq->objPmNotify->errorMessage;
                        $paymentTransactionStatusType = PaymentTransactionStatus::REJECTED;
                        $paymentStatusType            = PaymentStatus::USED;
                    }
                }
                catch(Exception $e)
                {
                    $errorType    = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_TEMPORARY;
                    $errorCode    = $e->getCode();
                    $errorMessage = $e->getMessage();
                }
            }
            else
            {
                $errorType    = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                $errorCode    = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_PARAMETERS;
                $errorMessage = 'mobilpay.ro posted invalid parameters';
            }
        }
        else
        {
            $errorType    = Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
            $errorCode    = Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_METHOD;
            $errorMessage = 'invalid request metod for payment confirmation';
        }

        if (null !== $orderId)
        {
            $paymentStatus =
                $this->getPaymentStatusRepo()->findOneById($paymentStatusType);
            $paymentTransactionStatus =
                $this->getPaymentTransactionStatusRepo()->findOneById($paymentTransactionStatusType);

            $payment = $this->getPaymentRepo()->findOneBy(
                ['orderId' => $orderId]
            );

            if (null !== $payment){
                $payment->setStatus($paymentStatus);
                $payment->setTransactionStatus($paymentTransactionStatus);

                $this->getEntityManager()->persist($payment);
                $this->getEntityManager()->flush();
            } else {
                #TODO return error
            }
        }

        $status = [
            'payment_status_id' => $paymentStatusType
        ];

        return new JsonResponse($status);
    }

    /**
    * @Route("/return", name="return")
    * @Method("GET")
    */
    public function paymentReturnAction(Request $request)
    {
        $orderId = $request->get('orderId');

        $payment = $this->getPaymentRepo()->findOneBy(
            ['orderId' => $orderId]
        );

        $this->sendPaymentDoneMessage($payment);

        $orderInfo = [
            'orderId' => $orderId
        ];

        return $this->render('QubizMobilePayBundle:Default:return.html.twig', $orderInfo);
    }

    public function sendPaymentDoneMessage($payment)
    {
        $user = $payment->getUser();
        $message = [
            'title'             => '1on1 payment OK message',
            'subtitle'          => '1on1 payment OK message',
            'body'              => '1on1 payment OK. Thank you!',
            'notification_type' => 'payment_done',
            'click_action'      => 'customUIButtons'
        ];

        $extraMessage = [
            'payment_status' => $payment->getTransactionStatus()->getName()
        ];

        return $this->getFirebaseNotification()->sendNotification(
            $user->getFirebaseToken(),
            $user->getDeviceType(),
            $message,
            $extraMessage
        );
    }

    public function refundTransaction($orderId = null)
    {
        try
        {
            //live mode WSDL location
            //$soap = new SoapClient('https://secure.mobilpay.ro/api/payment2/?wsdl', Array('cache_wsdl' => WSDL_CACHE_NONE));
            //test mode WSDL location
            $soap = new SoapClient(
                'http://sandboxsecure.mobilpay.ro/api/payment2/?wsdl',
                Array('cache_wsdl' => WSDL_CACHE_NONE)
            );

            $loginReq = new stdClass();

            $loginReq->username = $this->apiUserName;
            $loginReq->password = $this->apiPassword;

            $loginResponse = $soap->logIn(Array('request' => $loginReq));
            $sessId = $loginResponse->logInResult->id;

            $sacId = $this->apiAccessKey;


            // Credit example
            $req = new stdClass();
            $req->sessionId = $sessId; //the id we previously got from the login method
            $req->sacId = $sacId;
            $req->orderId = $orderId;
            $req->amount = '20'; // amount to credit



            try
            {
                $response = $soap->credit(Array('request' => $req)); //credit
        //        $response = $soap->capture(Array('request' => $req)); //capture
                if ($response->code != ERR_CODE_OK)
                {
                    throw new Exception($response->code, $response->message);
                }
            }
            catch(SoapFault $e)
            {
                throw new Exception($e->faultstring, $e->faultcode, $e);
            }

        }
        catch(SoapFault $e)
        {
           throw new Exception((string)$e->faultstring, (int) $e->faultcode, $e);
        }
    }
}

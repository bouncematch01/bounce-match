<?php

namespace Qubiz\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('qubiz_user');

        $rootNode->children()
                   ->arrayNode('facebook')
                     ->children()
                       ->scalarNode('app_id')->isRequired(true)->cannotBeEmpty()->end()
                       ->scalarNode('secret')->isRequired(true)->cannotBeEmpty()->end()
                       ->scalarNode('graph_version')->isRequired(true)->cannotBeEmpty()->end()
                     ->end()
                   ->end()
                 ->end();

        return $treeBuilder;
    }
}

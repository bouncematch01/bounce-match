<?php

namespace Qubiz\UserBundle\Security;

use OAuth2\IOAuth2GrantUser;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2 as BaseOAuth2;
use OAuth2\OAuth2ServerException;
use FOS\OAuthServerBundle\Storage\OAuthStorage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintValidator;

class OAuth2 extends BaseOAuth2
{
    /**
     * @var string
     */
    private $network;

    /**
     * @var ConstraintValidator
     */
    private $validator;

    public function __construct(OAuthStorage $storage, $config = [], ConstraintValidator $validator)
    {
        parent::__construct($storage, $config);

        $this->validator = $validator;
    }

    public function grantAccessToken(Request $request = null, $network = null)
    {
        $this->network = $network;

        // $errors = $this->validator->validate($this->network);

        // TODO: use the custom validator
        // if (count($errors) > 0) {
        if (null !== $this->network && 'facebook' !== $this->network) {
            throw new OAuth2ServerException(
                self::HTTP_BAD_REQUEST,
                self::ERROR_INVALID_REQUEST,
                // (string) $errors
                'Invalid network parameter or missing'
            );
        }

        return parent::grantAccessToken($request);
    }

    /**
     * {@inheritdoc}
     */
    protected function grantAccessTokenUserCredentials(IOAuth2Client $client, array $input)
    {
        if (!($this->storage instanceof IOAuth2GrantUser)) {
            throw new OAuth2ServerException(self::HTTP_BAD_REQUEST, self::ERROR_UNSUPPORTED_GRANT_TYPE);
        }

        if (!$input['username'] || !$input['password']) {
            throw new OAuth2ServerException(
                self::HTTP_BAD_REQUEST,
                self::ERROR_INVALID_REQUEST,
                'Missing parameters. "username" and "password" required'
            );
        }

        if (null === $this->network) {
            $stored = $this->storage->checkUserCredentials($client, $input['username'], $input['password']);
        } else {
            $stored = $this->storage->checkSocialCredentials(
                $client,
                $input['username'],
                $input['password'],
                $this->network
            );
        }

        if (false === $stored) {
            throw new OAuth2ServerException(
                self::HTTP_BAD_REQUEST,
                self::ERROR_INVALID_GRANT,
                'Invalid username and password combination'
            );
        }

        return $stored;
    }
}

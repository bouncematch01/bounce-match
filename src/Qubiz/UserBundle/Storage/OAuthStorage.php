<?php

namespace Qubiz\UserBundle\Storage;

use AppBundle\Entity\City;
use AppBundle\Entity\Country;
use AppBundle\Entity\Location as Venue;
use AppBundle\Entity\Profile;
use AppBundle\Entity\User;
use AppBundle\Entity\UserStatus;
use AppBundle\Entity\XpLevel;
use Doctrine\ORM\EntityManager;
use Exception;
use Facebook\Facebook;
use Facebook\GraphNodes\GraphUser;
use FOS\OAuthServerBundle\Storage\OAuthStorage as BaseOAuthStorage;
use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\AuthCodeManagerInterface;
use FOS\OAuthServerBundle\Model\ClientInterface;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenManagerInterface;
use InvalidArgumentException;
use OAuth2\Model\IOAuth2Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class OAuthStorage extends BaseOAuthStorage
{
    const FACEBOOK_PROFILE  =
        '/me?fields=id,email,first_name,last_name,cover,picture.height(480).width(480),location{location}';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var GraphUser
     */
    private $socialDetails;

    /**
     * @var array
     */
    private $config;

    /**
     * @var faceBookLocationId
     */

    public function __construct(
        ClientManagerInterface $clientManager,
        AccessTokenManagerInterface $accessTokenManager,
        RefreshTokenManagerInterface $refreshTokenManager,
        AuthCodeManagerInterface $authCodeManager,
        UserProviderInterface $userProvider = null,
        EncoderFactoryInterface $encoderFactory = null,
        EntityManager $entityManager,
        ContainerInterface $container
    ) {
        parent::__construct(
            $clientManager,
            $accessTokenManager,
            $refreshTokenManager,
            $authCodeManager,
            $userProvider,
            $encoderFactory
        );

        $this->em        = $entityManager;
        $this->container = $container;

        $this->config = [
            'facebook_app_id'        => $this->container->getParameter('facebook.app_id'),
            'facebook_secret'        => $this->container->getParameter('facebook.secret'),
            'facebook_graph_version' => $this->container->getParameter('facebook.graph_version')
        ];
    }

    public function checkSocialCredentials(IOAuth2Client $client, $socialId, $socialToken, $network)
    {
        if (!$client instanceof ClientInterface) {
            throw new InvalidArgumentException('Client has to implement the ClientInterface');
        }

        // TODO: use the custom validator
        // $errors = $this->validator->validate($this->network);

        // if (count($errors) > 0) {
        if ('facebook' !== strtolower($network)) {
            throw new InvalidArgumentException('Invalid network');
        }

        try {
            // TODO: pass repo as service & define method in repo
            $user = $this->em->getRepository('AppBundle:User')
                             ->findOneBy([sprintf('%sId', strtolower($network)) => $socialId]);

        } catch(AuthenticationException $e) {
            throw new InvalidArgumentException('Invalid network');
        }

        if (null !== $user) {
            $encoder = $this->encoderFactory->getEncoder($user);

            if ($this->checkSocialAccessToken($socialId, $socialToken, $network)) {
                $this->updateSocialToken($user, $socialId, $socialToken, $network);

                return ['data' => $user];
            }
        } else if ($this->checkSocialAccessToken($socialId, $socialToken, $network)) {
            $user = $this->createProfileFromSocialDetails($socialId, $socialToken, $network);

            if (null !== $user) {
                $encoder = $this->encoderFactory->getEncoder($user);

                return ['data' => $user];
            }
        }

        return false;
    }

    private function checkSocialAccessToken($socialId, $socialToken, $network)
    {
        $function = 'check' . ucfirst($network) . 'AccessToken';

        return $this->$function($socialId, $socialToken);
    }

    private function checkFacebookAccessToken($socialId, $socialToken)
    {
        // TODO: refactor this into service
        $facebook = new Facebook([
          'app_id'                => $this->config['facebook_app_id'],
          'app_secret'            => $this->config['facebook_secret'],
          'default_graph_version' => $this->config['facebook_graph_version']
        ]);

        try {
            $result = $facebook->get(self::FACEBOOK_PROFILE, $socialToken);

            $this->socialDetails = $result->getGraphUser();

            if (!$this->socialDetails->getId() || $this->socialDetails->getId() != $socialId) {
                return false;
            }

            return true;
        } catch (Exception $e) {
            // TODO: update message thrown here -> crt looks like bad pass / user
            $this->container->get('logger')->error($e);

            return false;
        }
    }

    private function createProfileFromSocialDetails($socialId, $socialToken, $network)
    {
        $user = new User();

        $user->setUsername($this->socialDetails->getId());
        $user->setPassword($this->socialDetails->getId());
        $user->setEnabled(true);

        if (!is_null($this->socialDetails->getEmail())) {
            $user->setEmail($this->socialDetails->getEmail());
        } else {
            $user->setEmail($this->socialDetails->getId());
        }

        $user->setOnboarded(false);

        $userStatus = $this->em->getRepository('AppBundle:UserStatus')
                         ->findOneById(UserStatus::ACTIVE);
        $user->setStatus($userStatus);

        $xpLevel = $this->em->getRepository('AppBundle:XpLevel')
                         ->findOneById(XpLevel::NEW);
        $user->setXpLevel($xpLevel);

        $this->updateSocialToken($user, $socialId, $socialToken, $network);

        $profile = new Profile();

        $profile->setFirstName($this->socialDetails->getFirstName());
        $profile->setLastName($this->socialDetails->getLastName());

        if (null !== $this->socialDetails->getLocation()){
            if (!is_null($this->socialDetails->getLocation()->getLocation()->getCity())) {
                $profile->setCity($this->socialDetails->getLocation()->getLocation()->getCity());
            }

            if (!is_null($this->socialDetails->getLocation()->getLocation()->getCountry())) {
                $profile->setCountry($this->socialDetails->getLocation()->getLocation()->getCountry());
            }
        } else {
            $cityName    = $this->em
                                ->getRepository('AppBundle:City')
                                ->findOneById(City::DEFAULT_CITY_ID)
                                ->getName();
            $countryName = $this->em
                                ->getRepository('AppBundle:Country')
                                ->findOneById(Country::DEFAULT_COUNTRY_ID)
                                ->getName();

            $profile->setCity($cityName);
            $profile->setCountry($countryName);
        }

        if (!is_null($this->socialDetails->getPicture())) {
            // $profile->setImageUrl($this->socialDetails->getPicture()->getUrl());
            $profile->setImageUrl(
                'https://graph.facebook.com/' .
                $user->getFacebookId() .
                '/picture?width=480&height=480'
            );
        }

        $defaultVenue = $this->em->getRepository('AppBundle:Location')
                         ->findOneById(Venue::DEFAULT_VENUE_ID);
        $profile->setLocation($defaultVenue);

        $profile->setUser($user);

        $this->em->persist($profile);
        $this->em->flush();

        return $user;
    }

    private function updateProfileFromSocialDetails($user)
    {
        if (is_null($user)) {
            return;
        }

        $profile = $this->em->getRepository('AppBundle:Profile')->findOneByUser($user);

        if (NULL === $profile) {
            return;
        }

        if (!is_null($this->socialDetails->getPicture())) {
            // $profile->setImageUrl($this->socialDetails->getPicture()->getUrl());
            $profile->setImageUrl(
                'https://graph.facebook.com/' .
                $user->getFacebookId() .
                '/picture?width=480&height=480'
            );
        }

       if (null !== $this->socialDetails->getLocation()){
            if (!is_null($this->socialDetails->getLocation()->getLocation()->getCity())) {
                $profile->setCity($this->socialDetails->getLocation()->getLocation()->getCity());
            }

            if (!is_null($this->socialDetails->getLocation()->getLocation()->getCountry())) {
                $profile->setCountry($this->socialDetails->getLocation()->getLocation()->getCountry());
            }
        } else {
            $cityName    = $this->em
                                ->getRepository('AppBundle:City')
                                ->findOneById(City::DEFAULT_CITY_ID)
                                ->getName();
            $countryName = $this->em
                                ->getRepository('AppBundle:Country')
                                ->findOneById(Country::DEFAULT_COUNTRY_ID)
                                ->getName();

            $profile->setCity($cityName);
            $profile->setCountry($countryName);
        }

        $this->em->persist($profile);
        $this->em->flush();

        return $user;
    }

    private function updateSocialToken($user, $socialId, $socialToken, $network)
    {
        if (is_null($user)) {
            return;
        }

        $this->updateProfileFromSocialDetails($user);

        $socialIdSetter = 'set' . ucfirst($network) . 'Id';
        $socialAccessTokenSetter = 'set' . ucfirst($network) . 'AccessToken';

        $user->$socialIdSetter($socialId);
        $user->$socialAccessTokenSetter($socialToken);
        $this->em->persist($user);
        $this->em->flush();
    }
}

<?php

namespace Qubiz\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Network extends Constraint
{
    public $message = 'The network "%name%" is not supported.';
}
